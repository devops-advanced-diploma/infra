Gitlab runner Ansible role
=========

Role install and manage Gitlab runner.
Register runner by `manage_runners` tag.

Requirements
------------

Python standart libraries:
- os
- json
- re.

Role Variables
--------------

Variable | Type | Required | Default | Description
:-- | :--: | :------: | :-----: | :----------
`gitlab_runner_update` | boolean | yes | false | If true, role download latest gitlab-runner package and try to update
`gitlab_runner_register_as_user` | boolean | yes | false | If true, register runners in user mode
`gitlab_runner_download_ca_certificates` | boolean | yes | true | Download ca certificate files and add `--tls-ca-file` option to register runner
`gitlab_runner_common_url` | string | no | 'https://gitlab.com' | Common url for all runners in `gitlab_runner_runners`
`gitlab_runner_common_executor` | string | no | 'docker' | Common executor for all runners in `gitlab_runner_runners`
`gitlab_runner_runners` | list | yes | [] | List of objects with runner configuration

`gitlab_runner_runners` keys

Key | Type | Required | Default | Description
:-- | :--: | :------: | :-----: | :----------
name | string | yes | - | Runner exclusive name
token | string | yes | - | Runner token
url | string | no | - | Specify GitLab url. If not exists, role get `gitlab_runner_common_url` value
executor | string | no | - | Specify runner executor. If not exists, role get `gitlab_runner_common_executor` value.

The role try to get token from environment variables. Variable names `GITLAB_RUNNER_TOKENS` and contains JSON format key values. Key - runner name, value - runner token. For example, for runner with name `project_infra`, use
```shell
export GITLAB_RUNNER_TOKENS='{ "project_infra": "t0k3N", "project_service": "an0Th3rT0k3N" }'
```

You can specify another variables for each runner. Just change `-` to `_` in `gitlab-runner register` option and use it as key. The role support only string values. Example:
```yaml
gitlab_runner_runners:
  - name: project_infra
    docker_image: "python:3.10.0-alpine"
    run_untagged: ""
    tag_list: "docker,infra,python"
    docker_container_labels: '{"com.example.vendor": "Test", "product.version": "1.0", "product.name": "Ansible role"}'
```

Executors and minimal options

Executor type | Required options
:------------ | :---------------
docker | docker_image
shell | -
ssh | ssh_host, ssh_port, ssh_user, ssh_password, ssh_identity_file
docker+machine | docker_image
kubernetes | -
virtualbox | virtualbox_base_name, ssh_user, ssh_password, ssh_identity_file
parallels | _will be added later_
docker-windows | _will be added later_
docker-autoscaler | _will be added later_
instance | _will be added later_

Get all options by ```gitlab-runner register --help```

Example Playbook
----------------

`playbook.yml` example:
```yaml
---
- hosts: gitlab-runner
  gather_facts: true
  roles:
    - gitlab-runner
```

`hosts.yml` example:
```yaml
---
all:
  hosts:
    gitlab-runner:
      ansible_host: 192.168.1.1
      gitlab_runner_common_url: https://gitlab.com
      gitlab_runner_common_executor: docker
      gitlab_runner_runners:
        - name: project_infra
          docker_image: "python:3.10.0-alpine"
          run_untagged: ""
          tag_list: "docker,infra,python"
          docker_container_labels: '{"com.example.vendor": "Test", "product.version": "1.0", "product.name": "Ansible role"}'
  vars:
    ansible_connection: ssh
    ansible_port: 22
    ansible_user: ansible
    ansible_ssh_private_key_file: ~/.ssh/id_rsa
```

Install and manage runners example:
```shell
export RUNNER_PROJECT_INFRA_TOKEN=s3cretT0k3n
ansible-playbook -i hosts.yml playbook.yml
```

Run only manage runners example:
```shell
export RUNNER_PROJECT_INFRA_TOKEN=s3cretT0k3n
ansible-playbook \
  -i hosts.yml \
  --tags manage_runners \
  playbook.yml
```

Clear all runners from host example:
```shell
ansible-playbook \
  -i hosts.yml
  --tags manage_runners \
  -e "gitlab_runner_runners=[]" \
  playbook.yml
```

License
-------

BSD

Author Information
------------------

Sergei Krepski sergei@krepski.ru
