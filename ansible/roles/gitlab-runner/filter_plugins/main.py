#!/usr/bin/python3
from os import getenv
from json import loads
from re import compile

class FilterModule(object):
    def filters(self):
        return {
            'remove_ansi': self.remove_ansi,
            'str_to_dict': self.str_to_dict,
            'add_token_from_env': self.add_token_from_env
        }
    
    def remove_ansi(self, data: str) -> str:
        """
        Remove ansi symbols from string.
        """
        ansi_escape = compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')
        return ansi_escape.sub('', data)

    def str_to_dict(self, data: str, elements_separator: str = ' ', kv_separator: str = ':'):
        """
        Convent string to dict.
        """
        result = {}
        for element in data.split(elements_separator):
            if kv_separator in element:
                key, value = element.split(kv_separator)
                result[key.strip()] = value.strip()
        return result

    def add_token_from_env(self, data: dict, env_var_name: str) -> dict:
        """
        Add 'token' key value from environment by 'name'
        """
        if data.get('token', None) is not None:
            return data
        name = data.get('name', None)
        if not name:
            return data

        raw_values = getenv(env_var_name)
        if raw_values is None:
            return data
        values = loads(raw_values)
        value = values.get(name, None)
        if value is None:
            return data
        data['token'] = value
        return data
