Wireguard
=========

Ansible role install Wireguard host and configure clients.

Client configs stores in `{{ wireguard_dir }}/clients`. 

If some existing client not listed in `wireguard_clients` (check by `name` and `ip_address`) - keys, configs and peers in server config will be deleted.

If you need several Wireguard interfaces on host, create different inventory files with same `ansible_host` and run playbook for each ivnentory file. `wireguard_interface`, `wireguard_port`, `wireguard_host_cidr` must be unique for each wireguard interface.

Additionaly, role can download clients config in `wireguard_clients_download_dir` directory. 
To download clients config set up `wireguard_clients_download_dir` variable.

Role Variables
--------------

Variable | Type | Required | Default | Description
:------- | :--: | :------: | :-----: | :----------
wireguard_dir | string | yes | /etc/wireguard | Path to Wireguard config files
wireguard_interface | string | yes | wg0 | Wireguard interface name
wireguard_listen_address | string | no | - | Specify listen ip address. If not specified, use ansible interface address
wireguard_port | number | yes | 51820 | Wireguard listen port
wireguard_host_cidr | string | 192.168.2.1/24 | Wireguard interface CIDR
wireguard_keepalive | number | no | 10 | Period (in seconds) to sent keepalive packet to the server
wireguard_masquerade | boolean | yes | true | Masquerade Wireguard clients
wireguard_dns | string | no | - | DNS for Wireguard clients
wireguard_allowed_ips | list | yes | - | List of CIDRs available by Wireguard
wireguard_iptables_allow_incoming | boolean | no | false | Add allow incoming UDP firewall rule for `wireguard_port`
wireguard_clients | list | yes | - | List of objects with Wireguard clients data
wireguard_clients_download_dir | string | - | "" | Directory for download clients configs. No download if empty.

`wireguard_clients` object keys:
Key | Type | Required | Default | Description
:-- | :--: | :------: | :-----: | :----------
name | string | yes | - | Wireguard client name (use only for client config filename)
ip_address | ip | yes | - | Wireguard client IP (must be inside `wireguard_host_cidr`)
allowed_ips | list | no | - | List of CIDRs available by client (use as gateway to another network)

Role tags
----------------

Tag | Action
:-: | :-----
install | Only install Wireguard on target host
configure | Configure Wireguard interface and start it. Wureguard must be installed
restart | Restart Wireguard interface. Interface must be configured
download_clients_configs | Only download clients config on local host

Example Playbook
----------------

This role require `become: true` option.

`hosts.yml` example:
```yaml
---
all:
  vars:
    ansible_connection: ssh
    ansible_port: 22
    ansible_user: ansible
    ansible_ssh_private_key_file: ~/.ssh/id_rsa
  children:
    _wireguard_hosts:
      hosts:
        gateway_via_external:
          ansible_host: 11.12.13.14
          wireguard_interface: wg0
          wireguard_port: 51820
          wireguard_host_cidr: 192.168.2.1/24
          wireguard_masquerade: false
          wireguard_allowed_ips:
            - 10.10.10.0/24
          wireguard_dns: "10.10.10.1, example.org"
          wireguard_iptables_allow_incoming: true
          wireguard_clients:
            - name: gateway
              ip_address: 192.168.2.2
              allowed_ips:
                - 10.10.10.0/24
            - name: user
              ip_address: 192.168.2.3

        gateway:
          ansible_host: 22.33.44.55
          wireguard_interface: wg0
          wireguard_listen_address: 22.33.44.55
          wireguard_port: 51821
          wireguard_host_cidr: 192.168.3.1/24
          wireguard_masquerade: false
          wireguard_allowed_ips:
            - 10.1.1.0/24
          wireguard_dns: "10.1.1.254, example.org"
          wireguard_iptables_allow_incoming: false
          wireguard_clients:
            - name: gateway
              ip_address: 192.168.3.254
              allowed_ips:
                - 10.1.1.0/24
            - name: user
              ip_address: 192.168.3.2

        vpn:
          ansible_host: 33.44.55.66
          wireguard_interface: wg0
          wireguard_port: 51822
          wireguard_host_cidr: 192.168.4.1/24
          wireguard_masquerade: true
          wireguard_allowed_ips:
            - 0.0.0.0/0
          wireguard_dns: "8.8.8.8"
          wireguard_iptables_allow_incoming: true
          wireguard_clients:
            - name: user
              ip_address: 192.168.4.2
```

`playbook.yml` example:
```yaml
---
- hosts: _wireguard_hosts
  gather_facts: true
  become: true
  roles:
    - wireguard
```

Run example:
```shell
ansible-playbook -i hosts.yml playbook.yml
```

License
-------

BSD

Author Information
------------------

Sergei Krepski
sergei@krepski.ru
