Grafana-docker
=========

This role install and run as the service Grafana.
The role required Docker installed on target host.

Role Variables
--------------

Variable | Type | Required | Default | Description
:-- | :--: | :------: | :-----: | :----------
grafana_dir | string | yes | /opt/grafana | Grafana files path
grafana_tag | string | no | latest | Grafana docker image tag
grafana_port | number | yes | 3000 | Local publish port
grafana_datasources | list | no | '' | List of objects (keys `name`, `url`) with datasources for Grafana provisioning

Dependencies
------------

Example Playbook
----------------

`host.yml` example:
```yaml
---
all:
  hosts:
    grafana-host:
      ansible_host: 192.168.1.1
      ansible_connection: ssh
      ansible_port: 22
      ansible_user: ansible
      ansible_ssh_private_key_file: ~/.ssh/id_rsa
      #
      grafana_datasources:
        - name: VictoriaMetrics
          url: 'http://victoriametrics:8428'
```

`playbook.yml` example:
```yaml
---
- hosts: grafana-host
  gather_facts: true
  roles:
    - grafana-docker
```

Run example:
```shell
ansible-playbook -i hosts.yml playbook.yml
```

License
-------

BSD

Author Information
------------------

Sergei Krepski
sergei@krepski.ru
