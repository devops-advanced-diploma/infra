#!/usr/bin/python
class FilterModule(object):
    def filters(self):
        return {
            'snake2camel_keys': self.snake_to_camel_dict_keys,
            'pop_keys': self.pop_keys,
            'append_key': self.append_key
        }
    def snake_to_camel(self, s: str) -> str:
        """
        Convert string from snake_case to camelCase.
        """
        result = s.split('_')
        for i in range(len(result)):
            result[i] = result[i].title() if i > 0 else result[i]
        return ''.join(result)
        

    def snake_to_camel_dict_keys(self, d: dict) -> dict:
        """
        Return dict with camelCase keys.
        """
        result = {}
        for key, value in d.items():
            if isinstance(value, dict):
                value = self.snake_to_camel_dict_keys(value)
            result[self.snake_to_camel(key)] = value
        return result

    def pop_keys(self, d: dict, keys: list[str] | str) -> dict:
        """
        Return dict without specified keys ignoring keys in dict values.
        """
        result = {}
        if isinstance(keys, str):
            keys = [keys]
        for key, value in d.items():
            if key not in keys:
                result[key] = value
        return result

    def append_key(self, d: dict, new_key: str, from_key: str) -> dict:
        """
        Retrun dict with additional new_key with value from from_key.
        """
        d[new_key] = d[from_key]
        return d
