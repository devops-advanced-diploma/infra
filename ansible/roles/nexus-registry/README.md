# Nexus OSS Registry

Role create and manage Nexus3 OSS Registry.

## Requirements

No requirements.

## Role Variables

### Common variables

Variable | Type | Require | Default | Description
:------- | :--: | :-----: | :-----: | :----------
nexus_version | string | yes | "3.49.0-02" | Nexus OSS version
nexus_base_dir | string | yes | "/opt" | Path to target Nexus OSS directory
nexus_user | string | yes | "{{ ansible_user_id }}" | Specify system username running Nexus OSS
nexus_address | string | yes | "0.0.0.0" | Local listen address
nexus_port | number | yes | 8081 | Local listen port

### API variables

Variable | Type | Require | Default | Description
:------- | :--: | :-----: | :-----: | :----------
nexus_api_address | string | yes | "localhost" | Specify api address
nexus_ansible_admin_id | string | yes | "{{ ansible_user_id }}" | Specify API username
nexus_ansible_admin_password | string | yes | - | Specify API user password (recomended set vavriable by extra-vars)

### Users variables

Variable | Type | Require | Default | Description
:------- | :--: | :-----: | :-----: | :----------
nexus_delete_default_users | boolean | no | false | If true all user_ids that not in `local_users` will be deleted
nexus_default_users | list | no | `admin`, `anonymous` | User_ids listed in this variable will be not deleted. `ansible_admin_id` always added in this list by default
nexus_admin_password | string | yes | - | Change `admin` user_id password on first run if defined.

### Anonimous access

By default Anonimous access is disabled.
You need specify anonimous user_id to enable Anonimous access.

```yaml
nexus_anonimous_access:
  enabled: true
  user_id: anonimous
  realm_name: NexusAuthorizingRealm
```

### Managed variables

All managed variable object keys are same as API variables in snake_case.

### Blobs

`nexus_blobs` is list of objects with blobs.

> If existing blob not set in `nexus_blobs` it will be deleted.

> If blob have repositories it can't be deleted. Playbook will send error.

#### Blobs objects keys

Key | Type | Require | Description
:-- | :--: | :-----: | :----------
name | string | yes | A unique identifier for Blob
type | strign| yes | Blob type (`file`, `s3`, `azure`)
path | string | yes | An absolute path or a path relative to <data-directory>/blobs
soft_quota | object | no | Blob soft quota.
soft_quota.type | string | yes | Soft quota constraint type(`spaceRemainingQuota`, `spaceUsedQuota`).
soft_quota.limit | integer | no | Soft quota constraint Limit (in Bytes)

> Unset `soft_quota` to disable Soft quota for Blob.

`nexus_blobs` by default:
```yaml
blobs:
  - name: default
    type: file
    path: default
```

#### Variable examples

##### File blobs store

```yaml
nexus_blobs:
  - name: default
    type: file
    path: default
  - name: raw
    type: file
    path: raw
    soft_quota_type: spaceRemainingQuota
    soft_quota_limit: 10000 # 10 GB
```

##### s3 blob store

```yaml
nexus_blobs:
  - name: s3-bucket
    type: s3
    bucket_configuration:
      bucket:
        region: DEFAULT
        name: string
        prefix: string
        expiration: 3
      encryption:
        encryption_type: s3ManagedEncryption
        encryption_key: string
      bucket_security:
        access_key_id: string
        secret_access_key: string
        role: string
        session_token: string
      advanced_bucket_connection:
        endpoint: string
        signer_type: string
        force_path_style: true
        max_connection_pool_size: 0
```

##### Azure blob store

```yaml
nexus_blobs:
  - name: azure-blob
    type: azure
    soft_quota:
      type: spaceRemainingQuota
      limit: 0
    bucket_configuration:
      account_name: string
      container_name: z-li8wy8d3q7tmkk42pwthsj0x1pd7j691lac
      authentication:
        authentication_method: ACCOUNTKEY
        account_key: string
```

### repositories

`nexus_repositories` is list of objects with repositories.

#### Repositories objects keys

Key | Type | Require | Description
:-- | :--: | :-----: | :----------
name | string | yes | Repository name
format| string | yes | Repository [format](#available-repositories-formats)
kind | string | yes | Repository kind (`hosted`, `proxy`, `group`).
online | boolean | yes | If `true`, the repository accepts incoming requests
storage | object | yes | Object with blob store settings.
storage.blob_store_name | string | yes | Existing blob storage name.
storage.strict_content_type_validation | boolean | yes | Validate that all content uploaded to this repository is of a MIME type appropriate for the repository format
storage.write_policy | string | yes | Controls if deployments of and updates to artifacts are allowed (`allow`, `allow_once`, `deny`)

#### Available repositories formats

Format | hosted | group | proxy | Usage
:----- | :----: | :---: | :---: | :----------
maven | + | + | + | Java, C#, Ruby, Scala, etc.
apt | + | - | + | Debian, Ubuntu packages
raw | + | + | + | Raw files, OS images, etc.
npm | + | + | + | Node.js
nuget | + | + | + | .NET, C++
rubygems | + | + | + | Ruby
docker | + | + | + | Docker
yum | + | + | + | CentOS, RHEL, AlmaLinux, etc.
helm | + | - | + | Kubernetes
gitlfs | + | - | - | Git large file system
pypi | + | + | + | Python
conda | - | - | + | Anaconda (Python distribution)
conan | - | - | + | C/C++
r | + | + | + | R
cocoapods | - | - | + | Swift, Objective-C
go | - | + | + | Go (golang)
p2 | - | - | + | Eclipse
bower | + | + | + | Client-side Web development

Repository kinds:
- `hosted` repository that stores components in the repository manager as the authoritative location for these components
- `group` repository allow you to combine multiple repositories and other repository groups in a single repository
- `proxy` repository that is x§ to a remote repository.

All additional object data for different repositories formats and kinds read in Nexus 3 OSS API documentation.

#### Variable example

```yaml
nexus_repositories:
- name: iso-images
  format: raw
  kind: hosted
  online: true
  storage:
    blob_store_name: raw
    strict_content_type_validation: false
    write_policy: allow_once
  component:
    proprietary_components: true
  raw:
    content_disposition: ATTACHMENT
```

### privileges

`nexus_privileges` is list of objects with privileges.

> Privileges starts with `nx-` can't be deleted.

Key | Type | Require | Description
:-- | :--: | :-----: | :----------
name | string | yes | Privilege name
type | string | yes | Privilege type (`wildcard`, `application`, `script`, `repository-view`, `repository-admin`, `repository-content-selector`)
description | string | no | Privilege description
actions | list | yes | List of available actions (`BROWSE`, `READ`, `EDIT`, `ADD`, `DELETE`, `RUN`, `*` for all)

All additional object data for different privilages types read in Nexus 3 OSS API documentation.

#### Variable example

```yaml
nexus_privileges:
- name: org-repository-view-raw-iso-images
  type: repository-view
  description: Browse iso images from registry
  format: 'raw'
  repository: 'iso-images'
  actions:
    - BROWSE
    - READ
```

### roles

`nexus_local_roles` is list of objects with internal roles.

Key | Type | Require | Description
:-- | :--: | :-----: | :----------
name | string | yes | Role name
description | string | no | Role description
source | string | yes | Role source (`default` for internal roles)
privileges | list | optional | List of existing privilege names
roles | list | optional | List of existing role names

All additional object data for different roles read in Nexus 3 OSS API documentation.

#### Variable example
```yaml
nexus_local_roles:
- name: system-administrator
  description: Role for read raw images
  source: default
  privileges:
    - org-repository-view-raw-iso-images
```

### local_users

`nexus_local_users` is list of objects with local (not LDAP) users

Key | Type | Require | Description
:-- | :--: | :-----: | :----------
user_id | string | yes | User login
first_name | string | yes | User first name
last_name | string | yes | User last name
email_address | string | yes | User email
status | string | yes | User status (`active`, `inactive`)
source | string | yes | User source (`default` for internal users)
roles | list | yes | List of user roles
password | string optional | User password

> `password` is required for new users. If you not specify new user password role generate password and place it in `{{ nexus_base_dir }}/sonatype-work/nexus3/{{ user.user_id}}.password` file (mode `0600`, owner `{{ nexus_user }}`).

#### Variable example

```yaml
nexus_local_users:
  - user_id: sysadmin
    first_name: System
    last_name: Administrator
    email: sysadmin@example.org
    status: active
    source: default
    roles:
      - system-administrator
```


# Dependencies

No dependencies.

# Example Playbook

`hosts.yml` example:

```yaml
--
all:
  hosts:
    nexus-oss-registry:
      ansible_host: 192.168.1.1
  vars:
    ansible_connection: ssh
    ansible_port: 22
    ansible_user: ansible
    ansible_ssh_private_key_file: ~/.ssh/ansible_key
```

`vars.yml` example:

```yaml
nexus_blobs:
  - name: default
    type: file
    path: default
  - name: raw
    type: file
    path: raw
    soft_quota_type: spaceRemainingQuota
    soft_quota_limit: 10000 # 10 GB

nexus_repositories:
- name: iso-images
  format: raw
  kind: hosted
  online: true
  storage:
    blob_store_name: raw
    strict_content_type_validation: false
    write_policy: allow_once
  component:
    proprietary_components: true
  raw:
    content_disposition: ATTACHMENT

nexus_privileges:
- name: org-repository-view-raw-iso-images
  type: repository-view
  description: Browse iso images from registry
  format: 'raw'
  repository: 'iso-images'
  actions:
    - BROWSE
    - READ

nexus_local_roles:
- name: system-administrator
  description: Role for read raw images
  source: default
  privileges:
    - org-repository-view-raw-iso-images

nexus_local_users:
  - user_id: sysadmin
    first_name: System
    last_name: Administrator
    email: sysadmin@example.org
    status: active
    source: default
    roles:
      - system-administrator
```

`playbook.yml` example:
```yaml
---
- hosts: nexus-oss-registry
  gather_facts: true
  roles:
    - nexus-registry
```

First run example:
```shell
ansible-galaxy -i hosts.yml -e "@vars.yml" -e "nexus_ansible_admin_password=s3cr3t" playbook.yml
```

Only manage run example:
```shell
ansible-galaxy -i hosts.yml -e "@vars.yml" -t manage playbook.yml
```

# License

MIT

# Author Information

Sergei Krepski
sergei@krepski.ru
