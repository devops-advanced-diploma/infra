Node-exporter
=========

Role install and run as a service Node exporter.

Role Variables
--------------

Variable | Type | Require | Default | Description
:------- | :--: | :-----: | :-----: | :----------
node_exporter_version | string | yes | 1.5.0 | Node exporter version. Note, that no need 'v' at start.
node_exporter_user | string | yes | monitoring | System user name
node_exporter_dir | string | yes | /run/node_exporter | Path to node_exporter binary files
node_exporter_config | object | no | see defaults/main.yml | Key value objects with node_exporter running options.

See details for `node_exporter_config` variable in [node_exporter]() official documentation.

Dependencies
------------

No

Example Playbook
----------------

`hosts.yml` example:
```yaml
---
all:
  vars:
    ansible_connection: ssh
    ansible_port: 22
    ansible_user: ansible
    ansible_ssh_private_key_file: ~/.ssh/id_rsa
    node_exporter_config:
      web.listen-address: ':9100'   # Addresses on which to expose metrics and web interface
      collector.netclass.netlink:   # Use netlink to gather stats instead of /proc/net/dev.
      collector.filesystem.mount-points-exclude: "^(autofs|binfmt_misc|bpf|cgroup2?|configfs|debugfs|devpts|devtmpfs|fusectl|hugetlbfs|iso9660|mqueue|nsfs|overlay|proc|procfs|pstore|rpc_pipefs|securityfs|selinuxfs|squashfs|sysfs|tracefs|tmpfs)"
  children:
    node_exporters:
      hosts:
        monitoring:
          ansible_host: 192.168.1.1
```

`playbook.yml` example:
```yaml
---
- hosts: node_exporters
  gather_facts: true
  roles:
    - node-exporter
```

Run example:
```shell
ansible-playbook -i hosts.yml playbook.yml
```

License
-------

BSD

Author Information
------------------

Sergei Krepski sergei@krepski.ru
