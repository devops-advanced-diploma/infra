import logging
from functools import wraps
from os import path, getcwd
from typing import Callable


# string for logging format
_LOG_FORMAT = f"%(asctime)s - [%(levelname)s] - %(name)s: %(message)s"
_LOG_FORMATTER = logging.Formatter(_LOG_FORMAT)


class ModuleLogger(logging.Logger):
    """
    Child class for updating Logger.
    """
    def log_function(self, func: Callable):
        """
        Decorator for functions. Logging start and end of function call.
        TODO: add switching off timer
        """
        @wraps(func)
        def wrapper(*args, **kwargs):
            """
            Wrapper for function
            """
            self.debug(
                f'Func [{func.__name__}] starts '
                f'(args={args}, kwargs={kwargs}).'
            )
            result = func(*args, **kwargs)
            self.debug(
                f'Func [{func.__name__}] ends: return ({result}).'
            )
            return result
        return wrapper


def _get_file_handler(level: int) -> logging.FileHandler:
    """
    Return file handler (error.log) by level.
    """
    file_handler = logging.FileHandler(
        filename=path.join(getcwd(), 'error.log'),
        encoding='utf-8'
    )
    file_handler.setLevel(level)
    file_handler.setFormatter(_LOG_FORMATTER)
    return file_handler


def _get_stdout_handler(level: int) -> logging.StreamHandler:
    """
    Return stream handler by level.
    """
    stdout_handler = logging.StreamHandler()
    stdout_handler.setLevel(level)
    stdout_handler.setFormatter(_LOG_FORMATTER)
    return stdout_handler


def get_logger(
    name: str, 
    stdout_level: int=logging.INFO, 
    file_level: int=logging.WARNING
) -> ModuleLogger:
    """
    Return Module logger with stdout handler by level 
    and file handler by level.
    """
    module_logger = ModuleLogger(name)
    module_logger.setLevel(stdout_level)
    module_logger.addHandler(_get_file_handler(file_level))
    module_logger.addHandler(_get_stdout_handler(stdout_level))
    return module_logger
