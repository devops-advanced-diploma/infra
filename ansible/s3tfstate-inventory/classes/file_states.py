from os import path, walk

from classes.logger import get_logger


# Initiate logger
logger = get_logger(__name__)


class FileStates:
    """
    Class for host tfstate files.
    """
    def __init__(self, file_path: str, file_extension: str='') -> None:
        """
        Initiate class for search tfstate files on host. 
        """
        logger.debug(f'Initiate FileStates instance with {file_path}')
        if not path.exists(file_path):
            msg = f'Initiate FileState error: {file_path} not exists.'
            logger.critical(msg)
            raise FileNotFoundError(msg)
        self._path = file_path
        self._extension = file_extension
    
    def list_files(self) -> list[str]:
        """
        Return list of filepathes.
        """
        files = []
        for (dirpath, dirnames, filenames) in walk(self._path):
            for fn in filenames:
                if fn.endswith(self._extension):
                    files.append(path.join(dirpath,fn))
        return files
    
    def get_content(self, filepath: str) -> bytes:
        """
        Return file content by filepath.
        """
        try:
            with open(filepath, 'rb') as f:
                return f.read()
        except FileExistsError:
            msg = f'File {filepath} not exists.'
            logger.error(msg)
            raise FileNotFoundError(msg)
