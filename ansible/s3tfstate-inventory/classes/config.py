from os import getenv, path
from yaml import safe_load

from classes.logger import get_logger


# Initiate logger
logger = get_logger(__name__)


class Config:
    """
    Class for get config.
    """
    def __init__(self, config_path: str='') -> None:
        """
        Initiate Config instance with config path.
        """
        logger.debug(f'Initiate Config instance with {config_path}')
        if config_path == '':
            config_path = path.expanduser('~/.config/s3tfstate_inventory.yml')
        if not path.exists(config_path):
            msg = f'Error: config file [{config_path}] not exists.'
            logger.critical(msg)
            raise FileExistsError(msg)
        self._config_path = config_path

        with open(config_path, 'r') as f:
            self._config = safe_load(f.read())
        
        
        
    
    def __str__(self) -> str:
        return f'{self.type} config from {self._config_path}'

    @property
    def type(self) -> str:
        """
        Return config type: s3 or directory.
        """
        return self._get_str_key(['type'])
    
    @property
    def s3_config(self) -> dict:
        """
        Return S3 Bucket connection attributes.
        """
        if self.type == 's3':
            return {
                'bucket_name': self._get_str_key(
                    ['s3', 'bucket_name'], 
                    env_var='S3_BUCKET_NAME'
                ),
                'access_key': self._get_str_key(
                    ['s3', 'access_key'],
                    env_var='S3_BUCKET_ACCESS_KEY'
                ),
                'secret_key': self._get_str_key(
                    ['s3', 'secret_key'],
                    env_var='S3_BUCKET_SECRET_KEY'
                ),
                'endpoint_url': self._get_str_key(
                    ['s3', 'endpoint'],
                    env_var='S3_BUCKET_ENDPOINT'
                ),
                'region_name': self._get_str_key(
                    ['s3', 'region'],
                    env_var='S3_BUCKET_REGION'
                )
            }
        msg = f'Wrong property call: [s3_config] with type [{self.type}].'
        logger.warn(msg)
        return dict()

    @property
    def s3_keys_config(self) -> dict:
        """
        Return S3 Bucket start directory.
        """
        default = {
            'directory': '',
            'extension': ''
        }
        return self._get_dict_value(['s3'], default=default)

    @property
    def directory_path_config(self) -> dict:
        """
        Return directory files config.
        """
        file_path = self._get_str_key(['directory', 'path'])
        if not path.exists(file_path):
            msg = f'Config path {file_path} not exists'
            logger.critical(msg)
            raise FileNotFoundError(msg)
        return {
            'file_path': path.abspath(file_path),
            'file_extension': self._get_str_key(
                ['directory', 'extension'], 
                default='.tfstate'
            )
        }
    
    @property
    def instance_config(self) -> dict:
        """
        Return instance config dict.
        """
        default = {
            'types': [],
            'types_like': []
        }
        return self._get_dict_value(['instance'], default=default)
    
    @property
    def group_config(self) -> dict:
        """
        Return group config dict.
        """
        default = {
            'label_names': [],
            'label_separator': ',',
        }
        return self._get_dict_value(['group'], default=default)
    
    @property
    def group_vars(self) -> dict:
        """
        Return dict with group vars. 
        Each value contains list of dynamic var names.
        """
        return self._get_dict_value(
            ['group_vars'], default=dict(), raise_on_empty=False
        )
    
    @property
    def host_vars(self) -> dict:
        """
        Return dict with host vars. 
        Each value contains list of dynamic_vars.
        """
        return self._get_dict_value(
            ['host_vars'], default=dict(), raise_on_empty=False
        )

    def _get_str_key(self, keys: list[str], 
                     env_var: str='', default: str='') -> str:
        """
        Return config string value by key.
        env_value is preffer.
        If default is empty can raise ValueError.
        """
        if env_var != '':
            value = getenv(env_var)
            if value is not None and isinstance(value, str):
                return value
        value = self._config.copy()
        for key in keys:
            try:
                value = value[key]
            except KeyError:
                value = None
        if value is not None and isinstance(value, str):
            return value
        if default != '':
            return default
        msg = f'Config error: var {"->".join(keys)}'
        logger.error(msg)
        raise ValueError(msg)
        
    def _get_dict_value(self, keys: list[str], 
                        default: dict={}, raise_on_empty: bool=True) -> dict:
        """
        Return config dict value by key.
        If default is empty can raise ValueError.
        """
        value = self._config.copy()
        for key in keys:
            try:
                value = value[key]
            except KeyError:
                value = None
        if value is None and not raise_on_empty:
            value = dict()
        for key, default_value in default.items():
            if value.get(key, None) is None:
                value[key] = default_value
        if raise_on_empty and (not isinstance(value, dict) or len(value) == 0):
            msg = f'Config error: var {"->".join(keys)}'
            logger.error(msg)
            raise ValueError(msg)
        # Remove keys not in defaults
        if default:
            for key in (set(value.keys()) - set(default.keys())):
                value.pop(key)
        return value
