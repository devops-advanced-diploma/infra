from classes.logger import get_logger
from classes.config import Config
from classes.tfstate import TFState, TFStateComputeInstance
from json import dumps


# Initiate logger
logger = get_logger(__name__)


class Inventory:
    instances: dict = dict()
    outputs: dict = dict()
    groups: set = set()

    def __init__(self, config: Config) -> None:
        """
        Initiate Inventory instance with Config instance.
        """
        logger.debug(f'Initiate Inventory instance with {config}')
        match config.type:
            case 's3':
                from classes.s3_bucket import S3Bucket

                s3 = S3Bucket(**config.s3_config)
                contents = [s3.get_content(x) for x in s3.list_keys(**config.s3_keys_config)]
            case 'directory':
                from classes.file_states import FileStates

                fs = FileStates(**config.directory_path_config)
                contents = [fs.get_content(x) for x in fs.list_files()]
            case _:
                msg = f'Unknown config type ${config.type}'
                logger.critical(msg)
                raise ValueError(msg)
        self._config = config
        for content in contents:
            state = TFState(content)
            for instance in state.instances(
                **config.group_config, 
                **config.instance_config
            ):
                self.instances[instance.hostname] = instance
            self.outputs.update(state.outputs)
        self._add_vars_to_hosts()
    
    def __str__(self) -> str:
        return (
            f'Inventoty from {self._config.type} config '
            f'from {self._config._config_path}'
        )
    
    @property
    def group_structure(self) -> dict:
        """
        Return dict with group structure.
        """
        return {'children': list(self.groups)}
    
    @property
    def groups(self) -> set[str]:
        """
        Return list of group names.
        """
        groups = set()
        for instance in self.instances.values():
            instance_groups = instance.groups
            if len(instance_groups) == 0:
                groups.update(set('ungrouped'))
            else:
                groups.update(set(instance.groups))
        return groups
    
    def group_hosts(self, group: str) -> list[str]:
        """
        Return list of hosts in group.
        """
        result = []
        for instance in self.instances.values():
            if group in instance.groups:
                result.append(instance.hostname)
        return result
    
    def host_vars(self, host: str) -> dict:
        """
        Return dict with host vars.
        """
        return self.instances[host].vars
    
    def _add_vars_to_hosts(self) -> None:
        """
        Add host vars from config.
        """
        group_vars = {}
        host_vars = {}
        for _group, _vars in self._config.group_vars.items():
            _group_vars = dict()
            for _var in _vars:
                value = self.outputs.get(_var, None)
                if value:
                    _group_vars[_var] = value
            group_vars[_group] = _group_vars
        for _host, _vars in self._config.host_vars.items():
            _host_vars = dict()
            for _var in _vars:
                value = self.outputs.get(_var, None)
                if value:
                    _host_vars[_var] = value
            host_vars[_host] = _host_vars
        
        for group, vars in group_vars.items():
            for instance in self.instances.values():
                if group in instance.groups or group == 'all':
                    instance.add_vars(vars)
        
        for host, vars in host_vars.items():
            instance = self.instances.get(host, None)
            if instance is not None:
                instance.add_vars(vars)

    def host_json(self, host: str, pretty: bool=False) -> str:
        """
        Return host vars in JSON format.
        """
        instance = self.instances.get(host, None)
        if instance is None:
            logger.error(f'Unknown host {host}')
            return dumps({})
        return dumps(instance.vars, indent=pretty)
    
    def list_json(self, pretty: bool=False) -> str:
        """
        Return list of inventory in JSON format.
        """
        data = {
            '_meta': {
                'hostvars': {h: i.vars for h, i in self.instances.items()}
            },
            'all': self.group_structure
        }
        for group in self.groups:
            data[group] = {'hosts': self.group_hosts(group)}
        return dumps(data, indent=pretty)
