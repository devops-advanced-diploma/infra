from boto3 import Session
from botocore import exceptions

from classes.logger import get_logger


# Initiate logger
logger = get_logger(__name__)


class S3Bucket:
    """
    Class for S3 Bucket connection.
    """
    def __init__(self, 
                 bucket_name: str, 
                 access_key: str, 
                 secret_key: str,
                 endpoint_url: str,
                 region_name: str) -> None:
        """
        Initiate S3 Bucket Instance.
        """
        logger.debug(f'Initiate S3Bucket instance {bucket_name}')
        self.bucket_name = bucket_name
        session = Session()
        self._client = session.client(
            service_name='s3',
            region_name=region_name,
            endpoint_url=endpoint_url,
            aws_access_key_id=access_key,
            aws_secret_access_key=secret_key,
            verify=True,
            use_ssl=True
        )
    
    def __str__(self) -> str:
        return f'S3 bucket {self.bucket_name}'
    
    def list_keys(self, directory: str='', extension: str='') -> list[str]:
        """
        Return list of keys in directory with extension.
        """
        result = []
        try:
            object_keys = self._client.list_objects(
                Bucket=self.bucket_name
            )['Contents']
        except exceptions.ClientError as e:
            logger.error(e)
            raise ValueError(e)
        try:
            result = [x['Key'] for x in object_keys]
        except KeyError as e:
            logger.error(e)
            raise KeyError(e)
        if directory:
            result = [x for x in result if x.startswith(directory)]
        if extension:
            result = [x for x in result if x.endswith(extension)]
        return result
    
    def get_content(self, key: str) -> bytes:
        """
        Return object content by key.
        """
        try:
            content_response = self._client.get_object(
                Bucket=self.bucket_name,
                Key=key
            )
        except exceptions.ClientError as e:
            logger.error(e)
            raise ValueError(e)
        except exceptions.UnknownKeyError as e:
            logger.error(e)
            raise FileNotFoundError(e)
        try:
            return content_response['Body'].read()
        except KeyError as e:
            logger.error(e)
            raise KeyError(e)
