from classes.logger import get_logger

# initiate logger
logger = get_logger(__name__)


class TFStateManagedInstance:
    """
    Basic class for Terraform state instance
    """
    _required_attribute_keys = (
        'id',
        'description',
        'labels'
    )
    id: str = ''
    description: str = ''
    labels: dict = dict()
    
    def __init__(self, instance_data: dict, type: str) -> None:
        """
        Initiate Managed type instance with instance_data and type. 
        """
        logger.debug(f'Initiate {type} ManagedInstance')
        errors = []
        for key in self._required_attribute_keys:
            try:
                instance_data['attributes'][key]
            except KeyError as e:
                errors.append(e)
        if len(errors) > 0:
            msg = f'Error initiating {type} ManagedInstance: {", ".join(errors)}'
            logger.error(msg)
            raise ValueError(msg)
        self._attributes: dict = instance_data['attributes']
        self.type: str = type
        self.vars: dict = dict()
        self.groups: list[str] = list()

    @property
    def id(self) -> str:
        """
        Return instance id.
        """
        return self._attributes['id']

    @property
    def description(self) -> str:
        """
        Return instance description.
        """
        return self._attributes['description']
    
    @property
    def labels(self) -> dict:
        """
        Return instance labels content.
        """
        return self._attributes['labels']
    
    def is_label(self, label_name: str) -> bool:
        """
        Return is instance have the label_name.
        """
        return label_name in self.labels.keys()
    
    def in_labels(self, label_names: list[str]) -> bool:
        """
        Return True if instance have one of label_names
        """
        for label in label_names:
            if label in self.labels.keys():
                return True
        return False
    
    def is_type(self, type: str) -> bool:
        """
        Return is instance is type.
        """
        return self.type == type
    
    def is_type_like(self, type_template: str) -> bool:
        """
        Return is instance type is like type_template.
        """
        return type_template in self.type
    
    def groups_from_labels(self, 
                           label_names: list[str], 
                           label_separator: str) -> None:
        """
        Add groups from labels
        """
        for label_name in label_names:
            if label_name in self.labels:
                self.groups += self.labels[label_name].split(label_separator)
        if self.groups == ['']:
            self.groups = ['ungrouped']
    
    def add_vars(self, vars: dict) -> None:
        """
        Add vars to instance.
        """
        self.vars.update(vars)


class TFStateComputeInstance(TFStateManagedInstance):
    """
    Class for compute instances.
    """
    _required_attribute_keys = (
        'id',
        'description',
        'labels',
        'fqdn',
        'hostname',
        'network_interface',
        'status'
    )
    fqdn: str = ''
    hostname: str = ''
    status: str = ''
    ip_address: str = ''

    def __init__(self, instance_data: dict, type: str) -> None:
        super().__init__(instance_data, type)
        self.add_vars({
            'ansible_host': self.ip_address
        })
    
    @property
    def fqdn(self) -> str:
        """
        Return instance fqdn.
        """
        return self._attributes['fqdn']

    @property
    def hostname(self) -> str:
        """
        Return instance hostname.
        """
        return self._attributes['hostname']

    @property
    def status(self) -> str:
        """
        Return instance status.
        """
        return self._attributes['status']

    @property
    def ip_address(self) -> str:
        """
        Return instance nat_ip_addres if nat or return internal ip address.
        """
        if self._is_nat:
            return self._external_ip
        elif self._internal_ip:
            return self._internal_ip
        else:
            return self.fqdn
    
    @property
    def _internal_ip(self) -> str:
        """
        Return instance internal ip address.
        """
        for ni in self._attributes['network_interface']:
            try:
                if ni['ip_address']:
                    return ni['ip_address']
            except KeyError:
                pass
        return ''
    
    @property
    def _external_ip(self) -> str:
        """
        Return instance nat ip address.
        """
        for ni in self._attributes['network_interface']:
            try:
                if ni['nat_ip_address']:
                    return ni['nat_ip_address']
            except KeyError:
                pass
        return ''

    @property
    def _is_nat(self) -> bool:
        """
        Return instance nat status.
        """
        for ni in self._attributes['network_interface']:
            try:
                if ni['nat']:
                    return ni['nat']
            except KeyError:
                pass
        return False
