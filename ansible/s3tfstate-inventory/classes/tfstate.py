from json import loads

from classes.logger import get_logger
from classes.tfstate_instance import TFStateComputeInstance

# initiate logger
logger = get_logger(__name__)


class TFState:
    """
    Class for Terraform state file parser.
    """
    _require_keys = (
        'outputs',
        'resources',
        'version',
        'terraform_version',
        'serial',
        'lineage'
    )

    def __init__(self, content: bytes) -> None:
        """
        Initiate Terraform state class with content.
        """
        logger.debug(f'Initiate TFState instance')
        self._content = loads(content)
        errors = []
        for key in self._require_keys:
            try:
                self._content[key]
            except KeyError as e:
                errors.append(e)
        if len(errors) > 0:
            msg = f'Error initiating TFState: {", ".join(errors)}'
            logger.error(msg)
            raise ValueError(msg)

    @property
    def outputs(self) -> dict:
        """
        Return TFState outputs
        """
        return {k: v['value'] for k, v in self._content['outputs'].items()}
    
    def instances(self, label_names: list[str], label_separator: str, 
                  types: list[str]=[], 
                  types_like: list[str]=[]) -> list[TFStateComputeInstance]:
        """
        Return list of TFStateComputeInstance.
        """
        instances = []
        for resource in self._content['resources']:
            if resource['mode'] == 'managed' \
                and self._is_resource_type(resource, types, types_like):
                for instance_data in resource['instances']:
                    instance = TFStateComputeInstance(
                        instance_data, resource['type'])
                    if instance.in_labels(label_names):
                        instance.groups_from_labels(
                            label_names, label_separator
                        )
                        instances.append(instance)
        return instances
    
    def _is_resource_type(self, resource: dict, 
                          types: list[str]=[], 
                          types_like: list[str]=[]) -> bool:
        """
        Return if resource in type or type_like.
        """
        if types:
            return resource['type'] in types
        if types_like:
            for type in types_like:
                if type in resource['type']:
                    return True
        return False
        
