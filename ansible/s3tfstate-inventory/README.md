# S3TFstate Inventory

## Description

Ansible dynamic inventory for using Terraform state files from host or from S3 bucket.

## Installation

1. Create config file in `~/.config/s3tfstate_inventory.yml`.
Use `s3tfstate_inventory.yml.sample` file as sample and configuration documentation.
1. Create virtual environment and install requirements.
    > ```shell
    > python3 -m venv .venv
    > source .venv/bin/activate
    > python3 -m pip install -r requirements.txt --require-virtualenv
    > ```
1. Make main file executable.
    > ```shell
    > chmod +x s3tfstate-inventory/main.py
    > ```
1. Test inventory:
    > ```shell
    > ansible-inventory -i s3tfstate-inventory/main.py --list --pretty
    > ```

## Usage

### Using on host:
```shell
ansible all -i s3tfstate-inventory/main.py -m ping
```
or
```shell
ansible-playbook -i s3tfstate-inventory/main.py playbook.yml
```

### Using in Gitlab CI
```yaml
stages:
  - provisioning

variables:
  ANSIBLE_ROOT: "${CI_PROJECT_DIR}/ansible"

.ansible: &ansible
  image:
    name: python:3.10.0-alpine
  before_script:
    - apk update && apk add openssh-client openssl curl
    - /usr/local/bin/python3 -m pip install --upgrade pip
    - /usr/local/bin/python3 -m pip install -r $ANSIBLE_ROOT/requirements.txt
    - /usr/local/bin/python3 -m pip install -r $ANSIBLE_ROOT/s3tfstate-inventory/requirements.txt
    - chmod +x $ANSIBLE_ROOT/s3tfstate-inventory/main.py
    - chmod 400 "$ANSIBLE_PRIVATE_KEY"
    - cp "$ANSIBLE_ROOT/s3tfstate_inventory.yml ~/.config/"
    - ansible --version
  tags:
    - infrastructure

ansible provisioning:
  stage: provisioning
  <<: *ansible
  variables:
    ANSIBLE_ROLES_PATH: "${ANSIBLE_ROOT}/roles"
    ANSIBLE_INVENTORY: "${ANSIBLE_ROOT}/s3tfstate_inventory/main.py"
    ANSIBLE_PRIVATE_KEY_FILE: "$ANSIBLE_PRIVATE_KEY"
    ANSIBLE_HOST_KEY_CHECKING: "false"
    ANSIBLE_FORCE_COLOR: "true"
  script:
    - ansible --list-hosts all
    - ansible-playbook "$ANSIBLE_ROOT/playbook.yml"
```

## License

Copyright (c) 2023 Sergei Krepski

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
