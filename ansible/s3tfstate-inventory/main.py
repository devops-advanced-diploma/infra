#!/usr/bin/env python3

###
#
# Run chmod +x main.py before
#
###

import argparse

from classes.logger import get_logger
from classes.config import Config
from classes.inventory import Inventory

# Initiate logger
logger = get_logger(__name__)


def create_arg_parser() -> argparse.ArgumentParser:
    """
    Create argument parser object with arguments:
    pretty, list, host.
    """
    arg_parser = argparse.ArgumentParser(
        description=__doc__,
        prog=__file__
    )
    arg_parser.add_argument(
        '--pretty',
        action='store_true',
        default=False,
        help='Pretty print JSON'
    )
    arg_parser.add_argument(
        '--list',
        action='store',
        nargs='*',
        default='dummy',
        help='Show JSON of all managed hosts'
    )
    arg_parser.add_argument(
        '--host',
        action='store',
        help='Display vars related to the host'
    )
    return arg_parser

if __name__ == '__main__':
    arg_parser = create_arg_parser()
    
    config = Config()
    inventory = Inventory(config)
    
    try:
        args = arg_parser.parse_args()
        if args.host:
            print(inventory.host_json(args.host, args.pretty))
        elif len(args.list) >= 0:
            print(inventory.list_json(args.pretty))
        else:
            raise ValueError('Expecting either --host $HOSTNAME or --list')
    except ValueError:
        raise
