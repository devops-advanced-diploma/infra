#!/bin/bash -e

function terraform_apply() {
    read -p "Create infra $1 (only \"yes\" applied): " choise
    if [[ $choise == "yes" ]]; then
        terraform -chdir="./$1" apply .tfplan
        echo -e "Infra \"$1\" created."
    else
        echo "Create infra by running command:"
        echo -e "\tterraform -chdir="./$1" apply .tfplan"
    fi
}

function terraform_destroy() {
    echo -e "Destroing infra for \"$1\"...\n"
    echo $(pwd)
    # terraform -chdir="./$1" fmt
    # terraform -chdir="./$1" validate
    terraform -chdir="./$1" plan \
        -var-file="../vars/$1.tfvars" \
        -destroy \
        -out=".tfplan"
    terraform -chdir="./$1" apply .tfplan
    rm ./$1/.tfplan
    echo -e "Infra \"$1\" destroyed."
}

create=0
destroy=0
[[ $1 == "create" ]] && create=1
[[ $1 == "destroy" ]] && destroy=1

if [[ $create -eq 0 && $destroy -eq 0 ]];then
    echo "Error script calling:"
    echo -e "\t\"./manage_infra.sh create (force)\" to create infra. force is optional"
    echo -e "\t\"./manage_infra.sh destroy\" to destroy infra"
    exit 128
fi


# Check virtual environment
if [ ! -d .venv ]; then
    python3 -m venv .venv
    source .venv/bin/activate
fi

source .env # TEST: .env-test

echo -e "Checking secrets... "
if [ -z "${ANSIBLE_PUBLIC}" ]; then
    [ -d .ssh ] || mkdir .ssh
    [ -s .ssh/id_rsa ] || ssh-keygen -t ed25519 -f .ssh/id_rsa -q -N ""
    export ANSIBLE_KEY=$(cat .ssh/id_rsa)
    export ANSIBLE_PUBLIC=$(cat .ssh/id_rsa.pub)
fi

# check variables
vars=(
    "TF_VAR_cloud_id"
    "TF_VAR_folder_id"
    "TF_VAR_sa_name"
    "TF_VAR_sa_auth_key"
    "S3_BUCKET_NAME"
    "S3_BUCKET_ACCESS_KEY"
    "S3_BUCKET_SECRET_KEY"
    "LOCKER_ENDPOINT"
    "LOCKER_TABLE"
    "ANSIBLE_USER"
    "ANSIBLE_KEY"
    "ANSIBLE_PUBLIC"
    "DOMAIN_NAME"
)
var_errors=""
for var in ${vars[@]}; do
    [ -z "${!var}" ] && var_errors="${var_errors}\n\t${var}"
done
if [ ! -z "${var_errors}" ]; then
    echo -e "Please, set up variables:${var_errors}"
    exit 128
fi
export TF_VAR_s3_bucket_name=$S3_BUCKET_NAME
export TF_VAR_s3_bucket_access_key=$S3_BUCKET_ACCESS_KEY
export TF_VAR_s3_bucket_secret_key=$S3_BUCKET_SECRET_KEY
export TF_VAR_ansible_user=$ANSIBLE_USER
export TF_VAR_ansible_user_public=$ANSIBLE_PUBLIC
export TF_VAR_domain_name=$DOMAIN_NAME
export TF_VAR_local_domain_name_server=""
echo -e "Done.\n"

# Create infra
infras=(
    "network"
    "infra_instances"
)

cd ./terraform
if [[ $create -eq 1 ]]; then
    chmod +x yc_terraform_init.sh
    # Network
    module="network"
    echo -e "Creating infra for \"$module\"...\n"
    /bin/bash ./yc_terraform_init.sh ./$module $2
    terraform -chdir="./$module" fmt
    terraform -chdir="./$module" validate
    terraform -chdir="./$module" plan \
        -var-file="../vars/$module.tfvars" \
        -out=".tfplan"
    terraform_apply $module $2
    
    # Infra_instances
    module="infra_instances"
    echo -e "Creating infra for \"$module\"...\n"
    /bin/bash ./yc_terraform_init.sh ./$module $2
    terraform -chdir="./$module" fmt
    terraform -chdir="./$module" validate
    terraform -chdir="./$module" plan \
        -var-file="../vars/$module.tfvars" \
        -var 'vpn_as_local_dns=false' \
        -out=".tfplan"
    terraform_apply $module $2
else
    # destroy
    for ((i=${#infras[@]}-1; i>=0; i--)); do
        terraform_destroy "${infras[$i]}"
    done
    exit 1
fi

cd ..

# Ansible running
cd ./ansible

# Consul token
if [ -z "${CONSUL_TOKEN}" ]; then
    export CONSUL_TOKEN=$(openssl rand -base64 32)
fi

# Install requirements
pip install --upgrade pip
pip install --requirement ./requirements.txt
pip install --requirement ./s3tfstate-inventory/requirements.txt

# Configure Ansible inventory
chmod +x ./s3tfstate-inventory/main.py
cp ./s3tfstate_inventory.yml ~/.config/

# set Ansible variables
export ANSIBLE_INVENTORY="$(pwd)/s3tfstate-inventory/main.py"  # TEST: "$(pwd)/hosts.yml"
export ANSIBLE_PLAYBOOK_DIR="$(pwd)/playbooks"
export ANSIBLE_ROLES_PATH="$(pwd)/roles"
export ANSIBLE_HOST_KEY_CHECKING="false"
export ANSIBLE_PRIVATE_KEY_FILE="../.ssh/id_rsa"
export ANSIBLE_FORCE_COLOR="true"

# Install ansible dependensies roles
ansible-galaxy install -r ./roles/requirements.yml

ansible-inventory --list
ansible-playbook -u "$ANSIBLE_USER" ./playbooks/infra_script_playbook.yml

echo -e "--------------------\n\n\tDONE\n\n--------------------"

# Output variables
group_var_names=(
    "S3_BUCKET_NAME"
    "S3_BUCKET_ACCESS_KEY"
    "S3_BUCKET_SECRET_KEY"
    "ANSIBLE_USER"
    "ANSIBLE_KEY"
    "DOMAIN_NAME"
    "REPOSITORY_PORT"
    "REPOSITORY_NAME"
    "REPOSITORY_USERNAME"
    "REPOSITORY_PASSWORD"
    "REPOSITORY_URL"
)
infra_var_names=(
    "TF_VAR_cloud_id"
    "TF_VAR_folder_id"
    "TF_VAR_sa_name"
    "TF_VAR_sa_auth_key"
    "LOCKER_ENDPOINT"
    "LOCKER_TABLE"
    "REGISTRY_API_PASSWORD"
    "ANSIBLE_PUBLIC"
    "GITLAB_RUNNER_TOKENS"
    "CONSUL_TOKEN"
)

echo -e "\nGroup vars:\n"
for var in ${group_var_names[@]}; do
    echo -e "${var}\t${!var}"
done

echo -e "\nInfra project vars:\n"
for var in ${infra_var_names[@]}; do
    echo -e "${var}\t${!var}"
done
