# Skillbox Advanced Diploma

## Requirements

You need to have:
1. Active [Yandex Cloud](https://cloud.yandex.ru) account with billing information
1. Active domain name delegated to [Yandex](https://cloud.yandex.ru/docs/dns/concepts/dns-zone#public-zones)
1. Installed [Terraform](https://www.terraform.io) ([mirror](https://cloud.yandex.ru/docs/tutorials/infrastructure-management/terraform-quickstart#install-terraform) instructions)
1. Installed [Python](https://www.python.org)
1. Installed [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/index.html)
1. Active Account in [GitLab.com](https://gitlab.com).
1. GitLab folder named by your application.
1. Two projects named `infra` and `service` in folder.

## Configuration

### Yandex Cloud

1. Create folder for your project in Yandex Cloud.
1. Create service account with name `terraform`
1. Create service account key file for user `terraform` and download it
1. Create service account static access key. Save `access` and `secret` keys.
1. Set `editor` role for service account
1. Create `Object storage`. Save storage name.
1. Create YDB Database and table inside YDB Database. Save `Document API endpoint` and `table name`. (See [habr.com](https://habr.com/ru/post/711982/) for details).
1. Set `ydb.editor` role for service account.

### GitLab

1. Create group named as a project.
1. Create projects in group:
    - infra
    - service
1. Add runner for each group.

### Terraform

1. Fill variable files in `./terraform/vars/`

### Script call

1. Copy `.env.example`:
    > ```shell
    > cp .env.example .env
    > ```
1. Fill `.env` file with variables
1. Make `manage_infra.sh` executable
    > ```shell
    > chmod +x ./manage_infra.sh
    > ```
1. To create basic infra run command:
    > ```shell
    > ./manage_infra.sh create
    > ```
    > use additional `force` flag to reset terraform states
1. To destroy infra run command:
    > ```shell
    > ./manage_infra.sh destroy
    > ```
