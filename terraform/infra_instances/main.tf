### Datasource
data "yandex_compute_image" "vpn_image" {
  family = var.vpn_template.image_family
}

data "yandex_compute_image" "runner_image" {
  family = var.runner_template.image_family
}

data "yandex_compute_image" "repository_image" {
  family = var.repository_template.image_family
}

### Locals
locals {
  dns_address = cidrhost(var.v4_cidr_block, -2)
}

### Modules
module "infra_subnet" {
  source = "../_modules/yc-subnet"

  folder_id           = var.folder_id
  resources_prefix    = var.resources_prefix
  vpc_id              = data.terraform_remote_state.vpc.outputs.vpc_id
  zone                = var.zone
  v4_cidr_block       = var.v4_cidr_block
  domain_name         = var.domain_name
  route_table_id      = data.terraform_remote_state.vpc.outputs.vpc_route_table_id
  domain_name_servers = var.vpn_as_local_dns ? [local.dns_address] : null
}

module "vpn" {
  source = "../_modules/yc-instance"

  folder_id             = var.folder_id
  resources_prefix      = var.resources_prefix
  instances_num         = 1
  instances_description = "VPN instance. Use as VPN server and (optional) DNS server."
  platform_id           = var.vpn_template.platform_id
  image_id              = data.yandex_compute_image.vpn_image.id
  hostname              = "vpn"
  cores                 = var.vpn_template.cores
  ram                   = var.vpn_template.ram
  core_fraction         = var.vpn_template.core_fraction
  preemptible           = var.vpn_template.preemptible
  disk_size             = var.vpn_template.disk_size
  subnet_id             = module.infra_subnet.subnet_id
  nat                   = true
  ip_address            = local.dns_address
  userdata = templatefile("../userdata.tftpl", {
    user        = var.ansible_user,
    user_public = var.ansible_user_public
  })
  ansible_groups = var.vpn_template.ansible_groups
}

module "runner" {
  source = "../_modules/yc-instance"

  folder_id             = var.folder_id
  resources_prefix      = var.resources_prefix
  instances_num         = 1
  instances_description = "Gitlab runner instance."
  platform_id           = var.runner_template.platform_id
  image_id              = data.yandex_compute_image.runner_image.id
  hostname              = var.runner_template.hostname
  cores                 = var.runner_template.cores
  ram                   = var.runner_template.ram
  core_fraction         = var.runner_template.core_fraction
  preemptible           = var.runner_template.preemptible
  disk_size             = var.runner_template.disk_size
  subnet_id             = module.infra_subnet.subnet_id
  nat                   = true
  userdata = templatefile("../userdata.tftpl", {
    user        = var.ansible_user,
    user_public = var.ansible_user_public
  })
  ansible_groups = var.runner_template.ansible_groups
}

module "repository" {
  source = "../_modules/yc-instance"

  folder_id             = var.folder_id
  resources_prefix      = var.resources_prefix
  instances_num         = 1
  instances_description = "Nexus repository instance."
  platform_id           = var.repository_template.platform_id
  image_id              = data.yandex_compute_image.repository_image.id
  hostname              = var.repository_template.hostname
  cores                 = var.repository_template.cores
  ram                   = var.repository_template.ram
  core_fraction         = var.repository_template.core_fraction
  preemptible           = var.repository_template.preemptible
  disk_size             = var.repository_template.disk_size
  subnet_id             = module.infra_subnet.subnet_id
  nat                   = var.repository_template.nat
  userdata = templatefile("../userdata.tftpl", {
    user        = var.ansible_user,
    user_public = var.ansible_user_public
  })
  ansible_groups = var.repository_template.ansible_groups
}
