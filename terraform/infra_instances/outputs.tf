output "infra_subnet_id" {
  value       = module.infra_subnet.subnet_id
  description = "Infra subnet ID."
}

output "infra_subnet_v4_cidr_block" {
  value       = flatten(module.infra_subnet.subnet_v4_cidr_blocks)[0]
  description = "Infra subnet`v4_cidr_block`."
}

output "infra_subnet_domain_name" {
  value       = module.infra_subnet.subnet_domain_name
  description = "Infra subnet domain name."
}

output "vpn_internal_ip" {
  value       = module.vpn.internal_ips[0]
  description = "VPN external ip address."
}

output "runner_internal_ip" {
  value       = module.runner.internal_ips[0]
  description = "Runner external ip address."
}

output "repository_internal_ip" {
  value       = module.repository.internal_ips[0]
  description = "Repository external ip address."
}

output "vpn_external_ip" {
  value       = module.vpn.external_ips[0]
  description = "VPN external ip address."
}

output "runner_external_ip" {
  value       = module.runner.external_ips[0]
  description = "Runner external ip address."
}

output "repository_external_ip" {
  value       = module.repository.external_ips[0]
  description = "Repository external ip address."
}

output "infra_hosts_data" {
  value       = setunion(module.vpn.hosts_data, module.repository.hosts_data, module.runner.hosts_data)
  description = "List hosts data (name, ip, nat_ip.)"
}

output "infra_domain_name_servers" {
  value       = module.infra_subnet.dns_servers
  description = "List of Infra DNS servers."
}
