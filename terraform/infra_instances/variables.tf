# Common Yandex Cloud variables
variable "cloud_id" {
  type        = string
  nullable    = false
  description = "(Required) Yandex Cloud id."
}

variable "folder_id" {
  type        = string
  nullable    = false
  description = "(Required) Yandex Cloud folder id."
}

variable "sa_name" {
  type        = string
  nullable    = false
  description = "(Required) Yandex Cloud service account name."
}

variable "sa_auth_key" {
  type        = string
  nullable    = false
  description = "(Required) Yandex Cloud service account auth key file path."
}

variable "s3_bucket_name" {
  type        = string
  nullable    = false
  description = "(Required) Terraform state S3 bucket name."
}

variable "s3_bucket_access_key" {
  type        = string
  nullable    = false
  description = "(Required) Terraform state S3 bucket access key."
}

variable "s3_bucket_secret_key" {
  type        = string
  nullable    = false
  description = "(Required) Terraform state S3 bucket secret key."
}

variable "s3_bucket_vpc_key" {
  type        = string
  nullable    = false
  description = "(Required) Terraform state S3 bucket VPC key."
}

variable "zone" {
  type        = string
  nullable    = false
  default     = "ru-central1-a"
  description = "(Optional) Subnet zone (ru-central1-a by default)."
  validation {
    condition = contains([
      "ru-central1-a",
      "ru-central1-b",
      "ru-central1-c"
    ], var.zone)
    error_message = "Unknown zone name ${var.zone}."
  }
}

variable "resources_prefix" {
  type        = string
  nullable    = true
  description = "(Optional) Prefix for resource names."
}

variable "domain_name" {
  type        = string
  nullable    = true
  default     = null
  description = "(Optional) Public DNS domain name."
  validation {
    condition     = can(regex("^+[0-9a-z].+[0-9a-z]$", var.domain_name))
    error_message = "Domain name must be second level at least."
  }
}

variable "vpn_as_local_dns" {
  type        = bool
  default     = false
  description = "(Optional) Use vpn instance as local DNS server."
}

variable "v4_cidr_block" {
  type        = string
  nullable    = false
  description = "(Required) Subnet cidr."
  validation {
    condition     = can(cidrnetmask(var.v4_cidr_block))
    error_message = "Invalid cidr ${var.v4_cidr_block}."
  }
}

variable "vpn_template" {
  type = object({
    platform_id : string,
    cores : number,
    core_fraction : number,
    ram : number,
    preemptible : bool,
    disk_size : number,
    image_family : string
    ansible_groups : string
  })
  default = {
    platform_id    = "standard-v2",
    cores          = 2,
    core_fraction  = 100,
    ram            = 1,
    preemptible    = false,
    disk_size      = 10,
    image_family   = "ubuntu-2204-lts"
    ansible_groups = "vpn"
  }
  description = "(Optional) VPN server template."
}

variable "runner_template" {
  type = object({
    hostname : string,
    platform_id : string,
    cores : number,
    core_fraction : number,
    ram : number,
    preemptible : bool,
    disk_size : number,
    image_family : string,
    ansible_groups : string
  })
  default = {
    hostname       = "runner",
    platform_id    = "standard-v2",
    cores          = 2,
    core_fraction  = 100,
    ram            = 4,
    preemptible    = false,
    disk_size      = 15,
    image_family   = "ubuntu-2204-lts"
    ansible_groups = "docker,gitlab-runner"
  }
  description = "(Optional) Gitlab runner template."
}

variable "repository_template" {
  type = object({
    hostname : string,
    platform_id : string,
    cores : number,
    core_fraction : number,
    ram : number,
    preemptible : bool,
    nat : bool,
    disk_size : number,
    image_family : string,
    ansible_groups : string
  })
  default = {
    hostname       = "repo",
    platform_id    = "standard-v2",
    cores          = 4,
    core_fraction  = 100,
    ram            = 8,
    preemptible    = false,
    nat            = false,
    disk_size      = 50,
    image_family   = "ubuntu-2204-lts"
    ansible_groups = "nexus"
  }
  description = "(Optional) Repository instance template."
}

variable "ansible_user" {
  type        = string
  nullable    = false
  description = "(Required) Ansible user name."
}

variable "ansible_user_public" {
  type        = string
  nullable    = false
  description = "(Required) Ansible user public key."
}
