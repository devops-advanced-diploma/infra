# cloud_id = 
# folder_id = 
# sa_name = 
# sa_auth_key = 
# s3_bucket_name = 
# s3_bucket_access_key = 
# s3_bucket_secret_key = 
s3_bucket_vpc_key = "terraform/infra/network"
zone              = "ru-central1-a"
resources_prefix  = "infra"
# domain_name = 
vpn_as_local_dns = true
v4_cidr_block    = "192.168.254.240/28"
vpn_template = {
  platform_id    = "standard-v2",
  cores          = 2,
  core_fraction  = 5,
  ram            = 1,
  preemptible    = true,
  disk_size      = 10,
  image_family   = "ubuntu-2204-lts"
  ansible_groups = "_consul_instances,_consul_clients"
}
runner_template = {
  num            = 1,
  hostname       = "runner",
  platform_id    = "standard-v2",
  cores          = 2,
  core_fraction  = 5,
  ram            = 1,
  preemptible    = true,
  disk_size      = 10,
  image_family   = "ubuntu-2204-lts"
  ansible_groups = "_docker,_consul_instances,_consul_clients"
}
# runner_instance_nat = 

repository_template = {
  hostname       = "nexus",
  platform_id    = "standard-v2",
  cores          = 4,
  core_fraction  = 5,
  ram            = 8,
  preemptible    = true,
  nat            = false,
  disk_size      = 20,
  image_family   = "ubuntu-2204-lts"
  ansible_groups = "_consul_instances,_consul_clients"
}

# ansible_user = 
# ansible_user_public = 
