# cloud_id = 
# folder_id = 
# sa_name = 
# sa_auth_key = 
# s3_bucket_name = 
# s3_bucket_access_key = 
# s3_bucket_secret_key = 
s3_bucket_vpc_key   = "terraform/infra/network"
s3_bucket_infra_key = "terraform/infra/instances"
zone                = "ru-central1-a"
resources_prefix    = "monitoring"
# domain_name = 
# additional_domain_name_server = 
v4_cidr_block = "192.168.254.192/28"
# ansible_user = 
# ansible_user_public = 

# vminsert_template = {
#     num = 2
#     hostname = "vminsert"
#     platform_id = "standard-v2"
#     cores = 2
#     core_fraction = 5
#     ram = 4
#     preemptible = true
#     disk_size = 15
#     image_family = "ubuntu-2204-lts"
#     ansible_groups = "_vminsert,_consul_instances"
# }

vmstorage_template = {
  num            = 1
  hostname       = "vmstorage"
  platform_id    = "standard-v2"
  cores          = 2
  core_fraction  = 5
  ram            = 4
  preemptible    = true
  disk_size      = 15
  image_family   = "ubuntu-2204-lts"
  ansible_groups = "_vmstorage,_consul_instances,_consul_clients"
}

# vmselect_template = {
#     num = 1
#     hostname = "vmselect"
#     platform_id = "standard-v2"
#     cores = 2
#     core_fraction = 5
#     ram = 4
#     preemptible = true
#     disk_size = 15
#     image_family = "ubuntu-2204-lts"
#     ansible_groups = "_vmselect"
# }

vmagent_grafana_template = {
  hostname       = "monitor"
  platform_id    = "standard-v2"
  cores          = 2
  core_fraction  = 5
  ram            = 4
  preemptible    = true
  disk_size      = 15
  image_family   = "ubuntu-2204-lts"
  ansible_groups = "_vmagent,_consul_instances,_consul_clients"
}

vmsinglenode_ansible_groups      = "_victoriametrics,_consul_instances,_consul_clients"
vmsinglenode_port                = 8428
vminsert_port                    = 8480
vmstorage_port                   = 8481
vmselect_port                    = 8482
victoriametrics_healthcheck_path = "/-/healthy"
