# Common Yandex Cloud variables
variable "cloud_id" {
  type        = string
  nullable    = false
  description = "(Required) Yandex Cloud id."
}

variable "folder_id" {
  type        = string
  nullable    = false
  description = "(Required) Yandex Cloud folder id."
}

variable "sa_name" {
  type        = string
  nullable    = false
  description = "(Required) Yandex Cloud service account name."
}

variable "sa_auth_key" {
  type        = string
  nullable    = false
  description = "(Required) Yandex Cloud service account auth key file path."
}

variable "s3_bucket_name" {
  type        = string
  nullable    = false
  description = "(Required) Terraform state S3 bucket name."
}

variable "s3_bucket_access_key" {
  type        = string
  nullable    = false
  description = "(Required) Terraform state S3 bucket access key."
}

variable "s3_bucket_secret_key" {
  type        = string
  nullable    = false
  description = "(Required) Terraform state S3 bucket secret key."
}

variable "s3_bucket_vpc_key" {
  type        = string
  nullable    = false
  description = "(Required) Terraform state S3 bucket VPC key."
}

variable "s3_bucket_infra_key" {
  type        = string
  nullable    = false
  description = "(Required) Terraform state S3 bucket Infra key."
}

variable "zone" {
  type        = string
  nullable    = false
  default     = "ru-central1-a"
  description = "(Optional) Subnet zone (ru-central1-a by default)."
  validation {
    condition = contains([
      "ru-central1-a",
      "ru-central1-b",
      "ru-central1-c"
    ], var.zone)
    error_message = "Unknown zone name ${var.zone}."
  }
}

variable "resources_prefix" {
  type        = string
  nullable    = true
  description = "(Optional) Prefix for resource names."
}

variable "domain_name" {
  type        = string
  nullable    = true
  default     = null
  description = "(Optional) Public DNS domain name."
  validation {
    condition     = can(regex("^+[0-9a-z].+[0-9a-z]$", var.domain_name))
    error_message = "Domain name must be second level at least."
  }
}

variable "v4_cidr_block" {
  type        = string
  nullable    = false
  description = "(Required) Subnet cidr."
  validation {
    condition     = can(cidrnetmask(var.v4_cidr_block))
    error_message = "Invalid cidr ${var.v4_cidr_block}."
  }
}

variable "consul_cluster_size" {
  type        = number
  default     = 3
  nullable    = false
  description = "(Optional) Consul cluster size (3 or 5)."
  validation {
    condition = anytrue([
      var.consul_cluster_size == 3,
      var.consul_cluster_size == 5
    ])
    error_message = "Invalid Consul cluster size ${var.consul_cluster_size}"
  }
}

variable "consul_template" {
  type = object({
    hostname : string
    platform_id : string,
    cores : number,
    core_fraction : number,
    ram : number,
    preemptible : bool,
    disk_size : number,
    image_family : string
    ansible_groups : string
  })
  default = {
    hostname       = "consul"
    platform_id    = "standard-v2",
    cores          = 2,
    core_fraction  = 100,
    ram            = 1,
    preemptible    = false,
    disk_size      = 20,
    image_family   = "ubuntu-2204-lts"
    ansible_groups = "_consul_cluster"
  }
}

variable "ansible_user" {
  type        = string
  nullable    = false
  description = "(Required) Ansible user name."
}

variable "ansible_user_public" {
  type        = string
  nullable    = false
  description = "(Required) Ansible user public key."
}
