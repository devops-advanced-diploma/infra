### Datasource
data "yandex_compute_image" "consul_image" {
  family = var.consul_template.image_family
}

### Modules
module "consul_subnet" {
  source = "../_modules/yc-subnet"

  folder_id           = var.folder_id
  resources_prefix    = var.resources_prefix
  vpc_id              = data.terraform_remote_state.vpc.outputs.vpc_id
  zone                = var.zone
  v4_cidr_block       = var.v4_cidr_block
  domain_name         = var.domain_name
  route_table_id      = data.terraform_remote_state.vpc.outputs.vpc_route_table_id
  domain_name_servers = data.terraform_remote_state.infra.outputs.infra_domain_name_servers
}

module "consul" {
  source = "../_modules/yc-instance"

  folder_id             = var.folder_id
  resources_prefix      = var.resources_prefix
  instances_num         = var.consul_cluster_size
  instances_description = "Consul cluster instance."
  platform_id           = var.consul_template.platform_id
  image_id              = data.yandex_compute_image.consul_image.id
  hostname              = var.consul_template.hostname
  cores                 = var.consul_template.cores
  ram                   = var.consul_template.ram
  core_fraction         = var.consul_template.core_fraction
  preemptible           = var.consul_template.preemptible
  disk_size             = var.consul_template.disk_size
  subnet_id             = module.consul_subnet.subnet_id
  nat                   = false
  userdata = templatefile("../userdata.tftpl", {
    user        = var.ansible_user,
    user_public = var.ansible_user_public
  })
  ansible_groups = var.consul_template.ansible_groups
}
