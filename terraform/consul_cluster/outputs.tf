output "consul_subnet_id" {
  value       = module.consul_subnet.subnet_id
  description = "Consul subnet ID."
}

output "consul_subnet_v4_cidr_block" {
  value       = flatten(module.consul_subnet.subnet_v4_cidr_blocks)[0]
  description = "Consul network `v4_cidr_block`."
}

output "consul_subnet_domain_name" {
  value       = module.consul_subnet.subnet_domain_name
  description = "Consul subnet domain name."
}

output "consul_hosts_data" {
  value       = module.consul.hosts_data
  description = "List hosts data (name, ip, nat_ip.)"
}

output "consul_internal_ips" {
  value       = module.consul.internal_ips
  description = "List of Consul internal ip addresses."
}
