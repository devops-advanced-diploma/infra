output "vpc_id" {
  value       = module.vpc.vpc_id
  description = "Network ID."
}

output "vpc_public_dns_id" {
  value       = module.vpc.public_dns_id
  description = "public DNS zone ID."
}

output "vpc_dns_zone" {
  value       = module.vpc.public_dns_zone
  description = "Public DNS zone domain name."
}

output "vpc_route_table_id" {
  value       = module.vpc.route_table_id
  description = "Route Table ID."
}
