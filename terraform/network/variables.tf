# Common Yandex Cloud variables
variable "cloud_id" {
  type        = string
  nullable    = false
  description = "(Required) Yandex Cloud id."
}

variable "folder_id" {
  type        = string
  nullable    = false
  description = "(Required) Yandex Cloud folder id."
}

variable "sa_name" {
  type        = string
  nullable    = false
  description = "(Required) Yandex Cloud service account name."
}

variable "sa_auth_key" {
  type        = string
  nullable    = false
  description = "(Required) Yandex Cloud service account auth key file path."
}

variable "zone" {
  type        = string
  nullable    = false
  default     = "ru-central1-a"
  description = "(Optional) Subnet zone (ru-central1-a by default)."
  validation {
    condition = contains([
      "ru-central1-a",
      "ru-central1-b",
      "ru-central1-c"
    ], var.zone)
    error_message = "Unknown zone name ${var.zone}."
  }
}

variable "resources_prefix" {
  type        = string
  nullable    = true
  description = "(Optional) Prefix for resource names."
}

variable "vpc_subnet_routes" {
  type = list(object({
    destination_prefix = string
    next_hop_address   = string
  }))
  nullable    = true
  default     = null
  description = "Describe route preferences for subnet."
}

variable "domain_name" {
  type        = string
  nullable    = true
  default     = null
  description = "(Optional) Public DNS domain name."
  validation {
    condition     = can(regex("^+[0-9a-z].+[0-9a-z]$", var.domain_name))
    error_message = "Domain name must be second level at least."
  }
}

variable "private_dns_prefix" {
  type        = string
  nullable    = true
  default     = null
  description = "(Optional) If exists, create private DNS zone \"private_dns_prefix.domain_name\"."
}

variable "create_nat_gw" {
  type        = bool
  nullable    = false
  default     = false
  description = "(Optional) Create a NAT gateway for internet access from subnet."
}
