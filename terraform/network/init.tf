terraform {
  required_version = ">= 1.0.0"
  backend "s3" {
    region   = "ru-central1"
    endpoint = "storage.yandexcloud.net"
    key      = "terraform/infra/network"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "> 0.8"
    }
  }
}

provider "yandex" {
  cloud_id                 = var.cloud_id
  folder_id                = var.folder_id
  service_account_key_file = var.sa_auth_key
  zone                     = var.zone
}

data "yandex_iam_service_account" "sa" {
  name = var.sa_name
}
