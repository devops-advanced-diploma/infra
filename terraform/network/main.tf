### Locals
locals {
  private_dns_zone = alltrue([
    var.domain_name != null,
    var.private_dns_prefix != null
  ]) ? "${var.private_dns_prefix}.${var.domain_name}" : null
}

### Modules
module "vpc" {
  source = "../_modules/yc-vpc"

  folder_id        = var.folder_id
  resources_prefix = var.resources_prefix
  subnet_routes    = var.vpc_subnet_routes
  public_dns_zone  = var.domain_name
  private_dns_zone = local.private_dns_zone
  create_nat_gw    = var.create_nat_gw
}
