#!/bin/sh -e

force=$2

# check Terraform install
terraform -version >> /dev/null || ( echo "Error. Please, install Terraform first.";  exit 128 )

[ -z $1 ] && ( echo "Error. Please, specify terraform files folder name."; exit 128 )

if [ -d "$(pwd)/$1" ]; then
    terraform_dir="$(pwd)/$1"
else
    echo "Error. Specify existing terraform files directory."
    exit 128
fi

# configurate Terraform for Yandex Cloud
echo "Terraform configuration..."
if [ -s ~/.terraformrc ]; then
    mv ~/.terraformrc ~/.terraformrc.old
fi
cat <<EOF > ~/.terraformrc
provider_installation {
    network_mirror {
        url = "https://terraform-mirror.yandexcloud.net/"
        include = ["registry.terraform.io/*/*"]
    }
    direct {
        exclude = ["registry.terraform.io/*/*"]
    }
}
EOF
echo -e "Done.\n"

# Init terraform
echo "Terraform initiating in ${terraform_dir}."
if [ ! -z $force ]; then
    rm -rf "${terraform_dir}/.terraform"
    echo "Remove ${terraform_dir}/.terraform by force option."
fi

[ -d "${terraform_dir}/.terraform" ] || terraform -chdir="${terraform_dir}" init \
    -backend-config=bucket=${S3_BUCKET_NAME} \
    -backend-config=region=ru-central1 \
    -backend-config=access_key=${S3_BUCKET_ACCESS_KEY} \
    -backend-config=secret_key=${S3_BUCKET_SECRET_KEY} \
    -backend-config=dynamodb_endpoint=${LOCKER_ENDPOINT} \
    -backend-config=dynamodb_table=${LOCKER_TABLE}

echo "Terraform configurated!"
