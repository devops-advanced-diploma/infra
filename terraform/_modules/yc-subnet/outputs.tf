output "subnet_id" {
  description = "ID of the created subnet"
  value       = yandex_vpc_subnet.subnet.id
}

output "subnet_v4_cidr_blocks" {
  description = "List of `v4_cidr_blocks` used in the VPC network"
  value       = yandex_vpc_subnet.subnet.v4_cidr_blocks
}

output "subnet_domain_name" {
  description = "Subnet domain name"
  value       = yandex_vpc_subnet.subnet.dhcp_options[0].domain_name
}

output "dns_servers" {
  description = "List of DNS server for DHCP."
  value       = yandex_vpc_subnet.subnet.dhcp_options[0].domain_name_servers
}
