### Datasource
data "yandex_client_config" "client" {}

### Locals
locals {
  folder_id = var.folder_id == null ? data.yandex_client_config.client.folder_id : var.folder_id
  prefix    = length(var.resources_prefix) > 0 ? "${var.resources_prefix}-" : ""
}

### Resources
# Subnet
resource "yandex_vpc_subnet" "subnet" {
  name           = "${local.prefix}subnet"
  description    = "Subnet for zone ${var.zone} (${var.v4_cidr_block})."
  folder_id      = local.folder_id
  network_id     = var.vpc_id
  zone           = var.zone
  v4_cidr_blocks = [var.v4_cidr_block]
  route_table_id = var.route_table_id
  dhcp_options {
    domain_name         = var.domain_name == null ? "internal" : "${var.domain_name}"
    domain_name_servers = var.domain_name_servers == null ? [cidrhost(var.v4_cidr_block, 2)] : var.domain_name_servers
    ntp_servers         = var.ntp_servers == null ? ["193.67.79.202", "64.62.194.189", "194.190.168.1"] : var.ntp_servers
  }
}
