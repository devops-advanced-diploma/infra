# Common Yandex Cloud variables
variable "folder_id" {
  type        = string
  nullable    = true
  default     = null
  description = "(Required) Yandex Cloud folder id."
}

variable "resources_prefix" {
  type        = string
  nullable    = true
  description = "(Optional) Prefix for resource names."
}

# Subnet variables
variable "vpc_id" {
  type        = string
  nullable    = false
  description = "(Required) Network id."
}

variable "zone" {
  type        = string
  nullable    = false
  default     = "ru-central1-a"
  description = "(Optional) Subnet zone (ru-central1-a by default)."
  validation {
    condition = contains([
      "ru-central1-a",
      "ru-central1-b",
      "ru-central1-c"
    ], var.zone)
    error_message = "Unknown zone name ${var.zone}."
  }
}

variable "v4_cidr_block" {
  type        = string
  nullable    = false
  description = "(Required) Subnet cidr."
  validation {
    condition     = can(cidrnetmask(var.v4_cidr_block))
    error_message = "Invalid cidr ${var.v4_cidr_block}."
  }
}

variable "route_table_id" {
  type        = string
  nullable    = true
  default     = null
  description = "(OPtional) ID of route table."
}

variable "domain_name" {
  type        = string
  nullable    = true
  description = "(Optional) resources domain name."
  validation {
    condition     = can(regex("^+[0-9a-z].+[0-9a-z]$", var.domain_name))
    error_message = "Domain name error: must be second level at least."
  }
}

variable "domain_name_servers" {
  type        = list(string)
  default     = null
  nullable    = true
  description = "(Optional) Domain name servers to be added to DHCP options."
}

variable "ntp_servers" {
  type        = list(string)
  default     = null
  nullable    = true
  description = "(Optional) NTP Servers to be added to DHCP options."
}
