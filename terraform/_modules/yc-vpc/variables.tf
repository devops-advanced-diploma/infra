# Common Yandex Cloud variables
variable "folder_id" {
  type        = string
  nullable    = true
  default     = null
  description = "(Required) Yandex Cloud folder id."
}

variable "resources_prefix" {
  type        = string
  nullable    = true
  description = "(Optional) Prefix for resource names."
}

variable "subnet_routes" {
  type = list(object({
    destination_prefix = string
    next_hop_address   = string
  }))
  nullable    = true
  default     = null
  description = "Describe route preferences for subnet."
}

variable "public_dns_zone" {
  type        = string
  nullable    = true
  default     = null
  description = "(Optional) Public DNS domain name."
  validation {
    condition = anytrue([
      var.public_dns_zone == null,
      can(regex("^+[0-9a-z].+[0-9a-z]$", var.public_dns_zone))
    ])
    error_message = "Domain name must be second level at least."
  }
}

variable "private_dns_zone" {
  type        = string
  nullable    = true
  default     = null
  description = "(Optional) Private DNS domain name."
  validation {
    condition = anytrue([
      var.private_dns_zone == null,
      can(regex("^+[0-9a-z].+[0-9a-z]$", var.private_dns_zone))
    ])
    error_message = "Domain name must be second level at least."
  }
}

variable "create_nat_gw" {
  type        = bool
  nullable    = false
  default     = false
  description = "(Optional) Create a NAT gateway for internet access from subnet."
}
