### Datasource
data "yandex_client_config" "client" {}

### Locals
locals {
  folder_id    = var.folder_id == null ? data.yandex_client_config.client.folder_id : var.folder_id
  prefix       = length(var.resources_prefix) > 0 ? "${var.resources_prefix}-" : ""
  routes_count = var.create_nat_gw || var.subnet_routes != null ? 1 : 0
}

### Resources
# Network
resource "yandex_vpc_network" "network" {
  folder_id = local.folder_id
  name      = "${local.prefix}network"
}

# Routes
resource "yandex_vpc_gateway" "egress_gateway" {
  count = local.routes_count

  name = "${local.prefix}egress-gateway"
  shared_egress_gateway {}
}

resource "yandex_vpc_route_table" "route_table" {
  count = local.routes_count

  name       = "${local.prefix}route-table"
  network_id = yandex_vpc_network.network.id

  dynamic "static_route" {
    for_each = var.subnet_routes == null ? [] : var.subnet_routes
    content {
      destination_prefix = static_route.value["destination_prefix"]
      next_hop_address   = static_route.value["next_hop_address"]
    }
  }
  dynamic "static_route" {
    for_each = var.create_nat_gw ? yandex_vpc_gateway.egress_gateway : []
    content {
      destination_prefix = "0.0.0.0/0"
      gateway_id         = static_route.value.id
    }
  }
}

# DNS zone
resource "yandex_dns_zone" "public" {
  count = var.public_dns_zone == null ? 0 : 1

  name        = "${local.prefix}public-dns-zone"
  folder_id   = local.folder_id
  description = "Public DNS zone for ${var.public_dns_zone}."

  zone   = "${var.public_dns_zone}."
  public = true
}

resource "yandex_dns_zone" "private" {
  count = var.private_dns_zone == null ? 0 : 1

  name        = "${local.prefix}private-dns-zone"
  folder_id   = local.folder_id
  description = "Private DNS zone for ${var.private_dns_zone}."

  zone   = "${var.private_dns_zone}."
  public = true
}
