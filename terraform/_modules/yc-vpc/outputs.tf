output "vpc_id" {
  description = "ID of the created network"
  value       = yandex_vpc_network.network.id
}

output "public_dns_id" {
  description = "ID of public DNS zone"
  value       = try(yandex_dns_zone.public[0].id, null)
}

output "public_dns_zone" {
  description = "Public DNS zone domain name."
  value       = try(yandex_dns_zone.public[0].zone, null)
}

output "private_dns_id" {
  description = "ID of private DNS zone"
  value       = try(yandex_dns_zone.private[0].id, null)
}

output "private_dns_zone" {
  description = "Private DNS zone domain name."
  value       = try(yandex_dns_zone.private[0].zone, null)
}

output "route_table_id" {
  description = "ID of Route table."
  value       = try(yandex_vpc_route_table.route_table[0].id, "")
}
