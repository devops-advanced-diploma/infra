# Common Yandex Cloud variables
variable "folder_id" {
  type        = string
  nullable    = true
  default     = null
  description = "(Required) Yandex Cloud folder id."
}

variable "resources_prefix" {
  type        = string
  nullable    = true
  description = "(Optional) Prefix for resource names."
}

variable "lb_name" {
  type        = string
  nullable    = false
  description = "(Required) Resource prefix name."
}

# Target group variables
variable "target_hosts" {
  type = list(object({
    subnet_id : string
    ip_address : string
  }))
  nullable    = false
  description = "(Required) List of target groups."
}

variable "target_port" {
  type        = number
  nullable    = false
  description = "(Required) Target hosts port."
}

variable "target_healthcheck" {
  type        = string
  nullable    = false
  description = "(Required) Target hosts healthcheck address."
}

# Load balancer variables
variable "lb_listen_port" {
  type        = number
  nullable    = false
  description = "(Required) Load balancer listen port."
}

variable "subnet_id" {
  type        = string
  nullable    = false
  description = "(Required) Load balancer subnet ID."
}

variable "lb_ipv4_address" {
  type        = string
  nullable    = true
  default     = null
  description = "(Optional) Load balancer IP address."
}
