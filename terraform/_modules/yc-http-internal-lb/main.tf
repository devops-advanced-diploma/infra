### Datasource
data "yandex_client_config" "client" {}

### Resources
resource "yandex_lb_target_group" "target_group" {
  name      = "${var.resources_prefix}-tg-${var.lb_name}"
  region_id = "ru-central1"

  dynamic "target" {
    for_each = var.target_hosts
    content {
      subnet_id = target.value.subnet_id
      address   = target.value.ip_address
    }
  }
}

resource "yandex_lb_network_load_balancer" "lb" {
  name      = "${var.resources_prefix}-lb-${var.lb_name}"
  region_id = "ru-central1"

  type = "internal"

  listener {
    name        = "${var.resources_prefix}-lb-listener-${var.lb_name}"
    protocol    = "tcp"
    port        = var.lb_listen_port
    target_port = var.target_port

    internal_address_spec {
      subnet_id  = var.subnet_id
      address    = var.lb_ipv4_address
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.target_group.id
    healthcheck {
      name                = "${var.resources_prefix}-lb-healthcheck-${var.lb_name}"
      healthy_threshold   = 2
      interval            = 60
      timeout             = 30
      unhealthy_threshold = 10
      http_options {
        port = var.target_port
        path = var.target_healthcheck
      }
    }
  }
}
