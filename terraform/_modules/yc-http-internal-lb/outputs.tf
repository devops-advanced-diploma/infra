output "target_group_id" {
  value       = yandex_lb_target_group.target_group.id
  description = "Target group ID."
}

output "lb_id" {
  value       = yandex_lb_network_load_balancer.lb.id
  description = "Load balancer ID."
}

output "target_hosts_ips" {
  value       = yandex_lb_target_group.target_group.target.*.address
  description = "List of target hosts IP addresses."
}

output "listen_port" {
  value       = yandex_lb_network_load_balancer.lb.listener.*.target_port[0]
  description = "Load balancer listen port."
}

output "target_port" {
  value       = yandex_lb_network_load_balancer.lb.listener.*.port[0]
  description = "Target hosts listen port."
}

output "lb_ipv4_address" {
  value = flatten([
    for l in yandex_lb_network_load_balancer.lb.listener :
    l.internal_address_spec.*.address
  ])[0]
  description = "Load balancer IPv4 address."
}
