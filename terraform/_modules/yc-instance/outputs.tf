output "internal_ips" {
  description = "List of internal IP adresses."
  value       = yandex_compute_instance.instances[*].network_interface.0.ip_address
}

output "external_ips" {
  description = "List of external IP adresses."
  value       = yandex_compute_instance.instances[*].network_interface.0.nat_ip_address
}

output "hosts_data" {
  description = "List of objects with name, ip, nat_ip."
  value = flatten([
    for host in yandex_compute_instance.instances : [{
      name   = host.hostname
      ip     = host.network_interface.0.ip_address,
      nat_ip = host.network_interface.0.nat_ip_address
    }]
  ])
}
