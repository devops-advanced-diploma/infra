# Common Yandex Cloud variables
variable "folder_id" {
  type        = string
  nullable    = true
  default     = null
  description = "(Required) Yandex Cloud folder id."
}

variable "resources_prefix" {
  type        = string
  nullable    = true
  description = "(Optional) Prefix for resource names."
}

# Instances variables
variable "instances_num" {
  type        = number
  nullable    = false
  default     = 1
  description = "(Required) Number of instances."
  validation {
    condition     = var.instances_num > 0
    error_message = "Number of instances must be 1 at least."
  }
}

variable "instances_description" {
  type        = string
  nullable    = true
  default     = ""
  description = "(Optional) Instances description."
}

variable "platform_id" {
  type        = string
  nullable    = true
  default     = "standard-v2"
  description = "(Optional) Instances platform (\"standard-v1\", \"standard-v2\", \"standard-v3\")."
  validation {
    condition = contains([
      "standard-v1",
      "standard-v2",
      "standard-v3"
    ], var.platform_id)
    error_message = "Unknown platform id ${var.platform_id}."
  }
}

variable "image_id" {
  type        = string
  nullable    = false
  description = "(Required) Instance image id."
}

variable "hostname" {
  type        = string
  nullable    = false
  description = "(Required) Instance hostname."
}

variable "cores" {
  type        = number
  nullable    = true
  default     = 2
  description = "(Optional) Instances cores."
  validation {
    condition     = var.cores > 1
    error_message = "Must be two cores at least."
  }
}

variable "ram" {
  type        = number
  nullable    = true
  default     = 2
  description = "(Optional) Instance RAM in GB."
  validation {
    condition     = var.ram >= 1
    error_message = "Too small RAM."
  }
}

variable "core_fraction" {
  type        = number
  nullable    = true
  default     = 100
  description = "(Optional) Instance core fraction (5%, 20%, 50%, 100%)."
  validation {
    condition     = contains([5, 20, 50, 100], var.core_fraction)
    error_message = "Unknown core fraction ${var.core_fraction}."
  }
}

variable "preemptible" {
  type        = bool
  nullable    = false
  default     = false
  description = "(Optional) Instances preemptible (false or true)."
}

variable "disk_size" {
  type        = number
  nullable    = true
  default     = 10
  description = "(Optional) Boot disk size (GB)."
  validation {
    condition     = var.disk_size >= 5
    error_message = "Disk size must be 5 GB at least."
  }
}

variable "subnet_id" {
  type        = string
  nullable    = false
  description = "(Required) Subnet ID for instances."
}

variable "ip_address" {
  type        = string
  nullable    = true
  default     = ""
  description = "(Optional) Static IPv4 instance address."
  validation {
    condition = anytrue([
      var.ip_address == "",
      can(cidrnetmask("${var.ip_address}/32"))
    ])
    error_message = "Error IP address ${var.ip_address}."
  }
}

variable "nat" {
  type        = bool
  nullable    = false
  default     = false
  description = "(Optional) External IP for instances."
}

variable "userdata" {
  type        = string
  nullable    = true
  default     = null
  description = "(Optional) Instance userdata for cloud init."
}

variable "ansible_groups" {
  type        = string
  nullable    = true
  default     = ""
  description = "(Optional) Ansible group names separated by comma for ansible inventory."
}
