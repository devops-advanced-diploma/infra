### Datasource
data "yandex_client_config" "client" {}

### Locals
locals {
  folder_id = var.folder_id == null ? data.yandex_client_config.client.folder_id : var.folder_id
  prefix    = length(var.resources_prefix) > 0 ? "${var.resources_prefix}-" : ""
}

### Resources
# Compute instances
resource "yandex_compute_instance" "instances" {
  count = var.instances_num

  name        = "${local.prefix}${var.hostname}-0${count.index + 1}"
  hostname    = var.instances_num == 1 ? var.hostname : "${var.hostname}-0${count.index + 1}"
  description = var.instances_description

  platform_id = var.platform_id
  resources {
    cores         = var.cores
    core_fraction = var.core_fraction
    memory        = var.ram
  }
  scheduling_policy {
    preemptible = var.preemptible
  }
  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = var.image_id
      size     = var.disk_size
    }
  }
  network_interface {
    subnet_id  = var.subnet_id
    ipv4       = true
    ipv6       = false
    ip_address = var.ip_address == "" ? "" : var.ip_address
    nat        = var.nat
  }
  allow_stopping_for_update = true
  metadata = {
    user-data = var.userdata == null ? "" : var.userdata
  }
  labels = {
    ansible_groups = replace(replace(var.ansible_groups, " ", ""), ",", "/")
  }
}
