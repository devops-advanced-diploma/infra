# Common Yandex Cloud variables
variable "cloud_id" {
  type        = string
  nullable    = false
  description = "(Required) Yandex Cloud id."
}

variable "folder_id" {
  type        = string
  nullable    = false
  description = "(Required) Yandex Cloud folder id."
}

variable "sa_name" {
  type        = string
  nullable    = false
  description = "(Required) Yandex Cloud service account name."
}

variable "sa_auth_key" {
  type        = string
  nullable    = false
  description = "(Required) Yandex Cloud service account auth key file path."
}

variable "s3_bucket_name" {
  type        = string
  nullable    = false
  description = "(Required) Terraform state S3 bucket name."
}

variable "s3_bucket_access_key" {
  type        = string
  nullable    = false
  description = "(Required) Terraform state S3 bucket access key."
}

variable "s3_bucket_secret_key" {
  type        = string
  nullable    = false
  description = "(Required) Terraform state S3 bucket secret key."
}

variable "s3_bucket_vpc_key" {
  type        = string
  nullable    = false
  description = "(Required) Terraform state S3 bucket VPC key."
}

variable "s3_bucket_infra_key" {
  type        = string
  nullable    = false
  description = "(Required) Terraform state S3 bucket Infra key."
}

variable "zone" {
  type        = string
  nullable    = false
  default     = "ru-central1-a"
  description = "(Optional) Subnet zone (ru-central1-a by default)."
  validation {
    condition = contains([
      "ru-central1-a",
      "ru-central1-b",
      "ru-central1-c"
    ], var.zone)
    error_message = "Unknown zone name ${var.zone}."
  }
}

variable "resources_prefix" {
  type        = string
  nullable    = true
  description = "(Optional) Prefix for resource names."
}

variable "domain_name" {
  type        = string
  nullable    = true
  default     = null
  description = "(Optional) Public DNS domain name."
  validation {
    condition     = can(regex("^+[0-9a-z].+[0-9a-z]$", var.domain_name))
    error_message = "Domain name must be second level at least."
  }
}

variable "v4_cidr_block" {
  type        = string
  nullable    = false
  description = "(Required) Subnet cidr."
  validation {
    condition     = can(cidrnetmask(var.v4_cidr_block))
    error_message = "Invalid cidr ${var.v4_cidr_block}."
  }
}

variable "ansible_user" {
  type        = string
  nullable    = false
  description = "(Required) Ansible user name."
}

variable "ansible_user_public" {
  type        = string
  nullable    = false
  description = "(Required) Ansible user public key."
}

variable "vmselect_template" {
  type = object({
    num : number,
    hostname : string,
    platform_id : string,
    cores : number,
    core_fraction : number,
    ram : number,
    preemptible : bool,
    disk_size : number,
    image_family : string,
    ansible_groups : string
  })
  default = {
    num            = 0,
    hostname       = "vmselect",
    platform_id    = "standard-v2",
    cores          = 2,
    core_fraction  = 100,
    ram            = 4,
    preemptible    = false,
    disk_size      = 50,
    image_family   = "ubuntu-2204-lts",
    ansible_groups = "_vmselect"
  }
  description = "(Optional) VictoriaMetrics vminsert instances template"
}

variable "vmstorage_template" {
  type = object({
    num : number,
    hostname : string,
    platform_id : string,
    cores : number,
    core_fraction : number,
    ram : number,
    preemptible : bool,
    disk_size : number,
    image_family : string,
    ansible_groups : string
  })
  default = {
    num            = 0,
    hostname       = "vmstorage",
    platform_id    = "standard-v2",
    cores          = 2,
    core_fraction  = 100,
    ram            = 4,
    preemptible    = false,
    disk_size      = 50,
    image_family   = "ubuntu-2204-lts",
    ansible_groups = "_vmstorage"
  }
  description = "(Optional) VictoriaMetrics vmstorage instances template"
}

variable "vminsert_template" {
  type = object({
    num : number,
    hostname : string,
    platform_id : string,
    cores : number,
    core_fraction : number,
    ram : number,
    preemptible : bool,
    disk_size : number,
    image_family : string,
    ansible_groups : string
  })
  default = {
    num            = 0,
    hostname       = "vminsert",
    platform_id    = "standard-v2",
    cores          = 2,
    core_fraction  = 100,
    ram            = 4,
    preemptible    = false,
    disk_size      = 50,
    image_family   = "ubuntu-2204-lts",
    ansible_groups = "_vminsert"
  }
  description = "(Optional) VictoriaMetrics vminsert instances template"
}

variable "vmagent_grafana_template" {
  type = object({
    hostname : string,
    platform_id : string,
    cores : number,
    core_fraction : number,
    ram : number,
    preemptible : bool,
    disk_size : number,
    image_family : string,
    ansible_groups : string
  })
  default = {
    hostname       = "monitor",
    platform_id    = "standard-v2",
    cores          = 2,
    core_fraction  = 100,
    ram            = 2,
    preemptible    = false,
    disk_size      = 15,
    image_family   = "ubuntu-2204-lts",
    ansible_groups = "_vmagent,_grafana"
  }
  description = "(Optional) Template for instance with vmagent and Grafana."
}

variable "vmsinglenode_ansible_groups" {
  type        = string
  default     = "_victoriametrics"
  description = "Ansible groups for singlenode."
}

variable "vmsinglenode_port" {
  type        = number
  default     = 8428
  description = "(Optional) Victoriametris singlenode port."
}

variable "vminsert_port" {
  type        = number
  default     = 8480
  description = "(Optional) Victoriametrics cluster vminsert port"
}

variable "vmstorage_port" {
  type        = number
  default     = 8481
  description = "(Optional) Victoriametrics cluster vmstorage port."
}

variable "vmselect_port" {
  type        = number
  default     = 8482
  description = "(Optional) Victoriametrics cluster vmselect port."
}

variable "victoriametrics_healthcheck_path" {
  type        = string
  default     = "/-/healthy"
  description = "(Optional) victoriametrics healthcheck path."
}
