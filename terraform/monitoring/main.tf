### Datasource
data "yandex_compute_image" "vminsert_image" {
  family = var.vminsert_template.image_family
}

data "yandex_compute_image" "vmstorage_image" {
  family = var.vmstorage_template.image_family
}

data "yandex_compute_image" "vmselect_image" {
  family = var.vmselect_template.image_family
}

data "yandex_compute_image" "vmagent_image" {
  family = var.vmagent_grafana_template.image_family
}

### Locals
locals {
  vmsinglenode = (var.vmselect_template.num + var.vmstorage_template.num + var.vminsert_template.num) == 1

  victoriamentics_internal_ips = flatten(setunion(
    module.vminsert.*.internal_ips,
    module.vmstorage.*.internal_ips,
    module.vmselect.*.internal_ips
  ))

  vminsert_lb_create = !local.vmsinglenode && var.vminsert_template.num > 1
  vmselect_lb_create = !local.vmsinglenode && var.vmselect_template.num > 1

  vminsert_lb_hosts_data = local.vminsert_lb_create ? [{
    name   = "vminsert"
    ip     = module.vminsert_lb.0.lb_ipv4_address
    nat_ip = ""
  }] : []

  vmselect_lb_hosts_data = local.vmselect_lb_create ? [{
    name   = "vmselect"
    ip     = module.vmselect_lb.0.lb_ipv4_address
    nat_ip = ""
  }] : []
}

### Modules
module "monitoring_subnet" {
  source = "../_modules/yc-subnet"

  folder_id           = var.folder_id
  resources_prefix    = var.resources_prefix
  vpc_id              = data.terraform_remote_state.vpc.outputs.vpc_id
  zone                = var.zone
  v4_cidr_block       = var.v4_cidr_block
  domain_name         = var.domain_name
  route_table_id      = data.terraform_remote_state.vpc.outputs.vpc_route_table_id
  domain_name_servers = data.terraform_remote_state.infra.outputs.infra_domain_name_servers
}

module "vminsert" {
  source = "../_modules/yc-instance"
  count  = var.vminsert_template.num == 0 ? 0 : 1

  folder_id             = var.folder_id
  resources_prefix      = var.resources_prefix
  instances_num         = var.vminsert_template.num
  instances_description = "Victoriametrics cluster instances${local.vmsinglenode ? "" : " (vminsert role)"}."
  platform_id           = var.vminsert_template.platform_id
  image_id              = data.yandex_compute_image.vminsert_image.id
  hostname              = local.vmsinglenode ? "vicrotiametrics" : var.vminsert_template.hostname
  cores                 = var.vminsert_template.cores
  ram                   = var.vminsert_template.ram
  core_fraction         = var.vminsert_template.core_fraction
  preemptible           = var.vminsert_template.preemptible
  disk_size             = var.vminsert_template.disk_size
  subnet_id             = module.monitoring_subnet.subnet_id
  nat                   = false
  userdata = templatefile("../userdata.tftpl", {
    user        = var.ansible_user,
    user_public = var.ansible_user_public
  })
  ansible_groups = local.vmsinglenode ? var.vmsinglenode_ansible_groups : var.vminsert_template.ansible_groups
}

module "vmstorage" {
  source = "../_modules/yc-instance"
  count  = var.vmstorage_template.num == 0 ? 0 : 1

  folder_id             = var.folder_id
  resources_prefix      = var.resources_prefix
  instances_num         = var.vmstorage_template.num
  instances_description = "Victoriametrics cluster instances${local.vmsinglenode ? "" : " (vmstorage role)"}."
  platform_id           = var.vmstorage_template.platform_id
  image_id              = data.yandex_compute_image.vmstorage_image.id
  hostname              = local.vmsinglenode ? "vicrotiametrics" : var.vmstorage_template.hostname
  cores                 = var.vmstorage_template.cores
  ram                   = var.vmstorage_template.ram
  core_fraction         = var.vmstorage_template.core_fraction
  preemptible           = var.vmstorage_template.preemptible
  disk_size             = var.vmstorage_template.disk_size
  subnet_id             = module.monitoring_subnet.subnet_id
  nat                   = false
  userdata = templatefile("../userdata.tftpl", {
    user        = var.ansible_user,
    user_public = var.ansible_user_public
  })
  ansible_groups = local.vmsinglenode ? var.vmsinglenode_ansible_groups : var.vmstorage_template.ansible_groups
}

module "vmselect" {
  source = "../_modules/yc-instance"
  count  = var.vmselect_template.num == 0 ? 0 : 1

  folder_id             = var.folder_id
  resources_prefix      = var.resources_prefix
  instances_num         = var.vmselect_template.num
  instances_description = "Victoriametrics cluster instances${local.vmsinglenode ? "" : " (vmselect role)"}."
  platform_id           = var.vmselect_template.platform_id
  image_id              = data.yandex_compute_image.vmselect_image.id
  hostname              = local.vmsinglenode ? "vicrotiametrics" : var.vmselect_template.hostname
  cores                 = var.vmselect_template.cores
  ram                   = var.vmselect_template.ram
  core_fraction         = var.vmselect_template.core_fraction
  preemptible           = var.vmselect_template.preemptible
  disk_size             = var.vmselect_template.disk_size
  subnet_id             = module.monitoring_subnet.subnet_id
  nat                   = false
  userdata = templatefile("../userdata.tftpl", {
    user        = var.ansible_user,
    user_public = var.ansible_user_public
  })
  ansible_groups = local.vmsinglenode ? var.vmsinglenode_ansible_groups : var.vmselect_template.ansible_groups
}

module "vmagent_grafana" {
  source = "../_modules/yc-instance"

  folder_id             = var.folder_id
  resources_prefix      = var.resources_prefix
  instances_num         = 1
  instances_description = "Vmagent and Grafana (in Docker) instance."
  platform_id           = var.vmagent_grafana_template.platform_id
  image_id              = data.yandex_compute_image.vmagent_image.id
  hostname              = var.vmagent_grafana_template.hostname
  cores                 = var.vmagent_grafana_template.cores
  ram                   = var.vmagent_grafana_template.ram
  core_fraction         = var.vmagent_grafana_template.core_fraction
  preemptible           = var.vmagent_grafana_template.preemptible
  disk_size             = var.vmagent_grafana_template.disk_size
  subnet_id             = module.monitoring_subnet.subnet_id
  nat                   = false
  userdata = templatefile("../userdata.tftpl", {
    user        = var.ansible_user,
    user_public = var.ansible_user_public
  })
  ansible_groups = var.vmagent_grafana_template.ansible_groups
}

module "vminsert_lb" {
  count  = local.vminsert_lb_create ? 1 : 0
  source = "../_modules/yc-http-internal-lb"

  folder_id        = var.folder_id
  resources_prefix = var.resources_prefix
  lb_name          = "vminsert"
  target_hosts = [for ip in module.vminsert.*.internal_ips[0] : {
    subnet_id  = module.monitoring_subnet.subnet_id
    ip_address = ip
  }]
  target_port        = var.vminsert_port
  target_healthcheck = var.victoriametrics_healthcheck_path
  lb_listen_port     = var.vminsert_port
  subnet_id          = module.monitoring_subnet.subnet_id
  lb_ipv4_address    = null
}

module "vmselect_lb" {
  count  = local.vmselect_lb_create ? 1 : 0
  source = "../_modules/yc-http-internal-lb"

  folder_id        = var.folder_id
  resources_prefix = var.resources_prefix
  lb_name          = "vmselect"
  target_hosts = [for ip in module.vmselect.*.internal_ips[0] : {
    subnet_id  = module.monitoring_subnet.subnet_id
    ip_address = ip
  }]
  target_port        = var.vmselect_port
  target_healthcheck = var.victoriametrics_healthcheck_path
  lb_listen_port     = var.vmselect_port
  subnet_id          = module.monitoring_subnet.subnet_id
  lb_ipv4_address    = null
}
