# Resource IDs

output "monitoring_subnet_id" {
  value       = module.monitoring_subnet.subnet_id
  description = "Monitoring subnet ID."
}

output "vminsert_lb_id" {
  value       = try(module.vminsert_lb.0.lb_id, "")
  description = "vminsert load balancer ID."
}

output "vminsert_lb_target_group_id" {
  value       = try(module.vminsert_lb.0.target_group_id, "")
  description = "vminsert target group ID."
}

output "vmselect_lb_id" {
  value       = try(module.vmselect_lb.0.lb_id, "")
  description = "vmselect load balancer ID."
}

output "vmselect_lb_target_group_id" {
  value       = try(module.vmselect_lb.0.target_group_id, "")
  description = "vmselect target group ID."
}

# Networks

output "monitoring_subnet_v4_cidr_block" {
  value       = flatten(module.monitoring_subnet.subnet_v4_cidr_blocks)[0]
  description = "Monitoring network `v4_cidr_block`."
}

output "monitoring_subnet_domain_name" {
  value       = module.monitoring_subnet.subnet_domain_name
  description = "Monitoring subnet domain name."
}

# Host data

output "monitoring_hosts_data" {
  value = setunion(
    try(module.vminsert.0.hosts_data, []),
    try(module.vmstorage.0.hosts_data, []),
    try(module.vmselect.0.hosts_data, []),
    local.vminsert_lb_hosts_data,
    local.vmselect_lb_hosts_data,
    module.vmagent_grafana.hosts_data
  )
  description = "List hosts data (name, ip, nat_ip.)"
}

# IP addresses

output "victoriametrics_internal_ips" {
  value       = local.victoriamentics_internal_ips
  description = "Victoriametrics cluster internal IP list."
}

output "vmagent_grafana_internal_ip" {
  value       = module.vmagent_grafana.internal_ips[0]
  description = "Vmagent and metrics Grafana IP address."
}

output "vminsert_internal_ip" {
  value       = local.vmsinglenode ? local.victoriamentics_internal_ips[0] : try(module.vminsert_lb.0.lb_ipv4_address, module.vminsert.0.internal_ips[0])
  description = "vminsert ip address."
}

output "vmstorage_internal_ips" {
  value       = try(module.vmstorage.*.internal_ips, [])
  description = "List of vmstorage ip addresses."
}

output "vmselect_internal_ip" {
  value       = local.vmsinglenode ? local.victoriamentics_internal_ips[0] : try(module.vmselect_lb.0.lb_ipv4_address, module.vmselect.0.internal_ips[0])
  description = "vmselect ip address."
}

# Ports

output "vminsert_listen_port" {
  value       = local.vmsinglenode ? var.vmsinglenode_port : try(module.vminsert_lb.0.listen_port, var.vminsert_port)
  description = "vminsert port"
}

output "vmstorage_listen_port" {
  value       = local.vmsinglenode ? var.vmsinglenode_port : var.vmstorage_port
  description = "vmstorage port"
}

output "vmselect_listen_port" {
  value       = local.vmsinglenode ? var.vmsinglenode_port : try(module.vmselect_lb.0.listen_port, var.vmselect_port)
  description = "vmselect port"
}
