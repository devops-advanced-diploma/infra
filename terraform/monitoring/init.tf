terraform {
  required_version = ">= 1.0.0"
  backend "s3" {
    region   = "ru-central1"
    endpoint = "storage.yandexcloud.net"
    key      = "terraform/infra/monitoring"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "> 0.8"
    }
  }
}

provider "yandex" {
  cloud_id                 = var.cloud_id
  folder_id                = var.folder_id
  service_account_key_file = var.sa_auth_key
  zone                     = var.zone
}

### Datasources
data "yandex_iam_service_account" "sa" {
  name = var.sa_name
}

data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket     = var.s3_bucket_name
    region     = "ru-central1"
    endpoint   = "storage.yandexcloud.net"
    key        = var.s3_bucket_vpc_key
    access_key = var.s3_bucket_access_key
    secret_key = var.s3_bucket_secret_key

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

data "terraform_remote_state" "infra" {
  backend = "s3"
  config = {
    bucket     = var.s3_bucket_name
    region     = "ru-central1"
    endpoint   = "storage.yandexcloud.net"
    key        = var.s3_bucket_infra_key
    access_key = var.s3_bucket_access_key
    secret_key = var.s3_bucket_secret_key

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}
